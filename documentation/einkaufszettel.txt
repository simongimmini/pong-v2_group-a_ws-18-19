Brett x 3:
    Hoch:   15 mm
    Lang:   560 mm 
    Breit:  76 mm
    
Fußklötze x 9:
    Breit: 66 mm
    Höhe/Länge: 20 mm
    
Mittelteil: 
    120 mm x 120 mm x 15 mm
    (Vllt etwas größer für Puffer, da eine Grundseite 12cm)

Scharniere x 6:
    Nicht breiter als 7 cm, es sei denn wir nehmen 2, dann nicht breiter als 3 cm
    
Trapezschutz für LED: (Vllt zuerst hier nach suchen, zum umdisponieren falls es das nicht gibt)
    Lang: 560 mm
    Kurze Seitenlängen: 200 mm