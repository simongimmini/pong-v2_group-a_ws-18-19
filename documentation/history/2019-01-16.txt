1. Löcher in Mitteldreieck und Leisten gebohrt, Füße geschliffen (diese und Dreieck wurden bereits zuhause gesägt), Abschlussstücke für Leistenenden gebaut
2. Die LEDs von den illuminated Buttons im Code implementieren (Code)
3. Buttons getestet
4. Scharniere an die Leisten angebracht

Für diesen Treffen wurden besorgt: Holzfüße, Farbe, Scharniere, Schrauben

TODO: - Besorge 3,18m (2x2m) Holzleisten, ca 0,5cm x 1,5cm, für neben der LED-Leiste
      - Löte die LED-Leisten
      - Wegen Kabeln (rot & schwarz) fragen, gibt es Konventionen die wir einhalten müssen? Gibt es diese Farben hier irgendwo?



Abends programmieren:
- determinieren wer anfängt: reaktionstest, wer zuerst klickt wenn led leuchtet fängt an
- win_animation, direction_selection, determine_start_player
- LED Pins(button) in buttoncontrol festgelegt

- Leiste verkürzt sich, wenn man ein Leben verliert (oder der mit dem meisten leben) [oder als powerup]
- Punkte werden blass am Anfang der Leiste angezeigt
- einfacher modus wie er schon in der anderen Gruppe gemacht worden ist
