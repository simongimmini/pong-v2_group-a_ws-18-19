\subsubsection{Allgemeine Informationen}
Bevor die Kommunikation zwischen MC und LEDs erläutert wird, werden zunächst einige allgemeine Informationen dargestellt.
Ein grober Überblick zeigt nun, welche Funktionen in welchen Dateien umgesetzt wurden, danach wird genauer auf eben diese Umsetzung eingegangen:

\begin{itemize}
	\item \texttt{ledcommunication} - Enthält sämtliche Funktionalitäten, welche für die Kommunikation zwischen MC und LEDs nötig sind.
	\item \texttt{buttoncontrol} - Enthält sämtliche Funktionalitäten, welche für die Kommunikation zwischen MC und den Buttons nötig sind.
	\item \texttt{1dpong} - Enthält die main-Funktion. Startet anfangs das Menü und ruft während des Spielens die einzelnen (ausgelagerten) Funktionen auf, die pro Modus benötigt werden.
	\item \texttt{menu} - Enthält die benötigten Funktionen um die Menüs zu starten und anzuzeigen.
	\item \texttt{gamecontrol} - Enthält die Funktionen, welche in allen Spielmodi, unabhängig von der Anzahl der Spieler, benötigt werden, z.B. die Funktion, die den Start-Spieler bestimmt.
	\item \texttt{oneplayer} - Kontrolliert die Einzelheiten des Einspielermodus'.
	\item \texttt{twoplayer} - Kontrolliert die Einzelheiten der Zweispielermodi.
	\item \texttt{threeplayer} - Kontrolliert die Einzelheiten der Dreispielermodi.
\end{itemize}

\pagebreak
\subsubsection {Definitionen}
Die Kommunikation zwischen dem MC und der LED-Leiste sind in den Dateien \dq ledcommunication.h\dq \ und \dq ledcommunication.c\dq \ umgesetzt.
In der Datei ledcommunication.h ist es zuerst nötig das Register, die Pins und die Ports der LED-Leisten zu definieren. Dies geschieht hier:
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
#define LED_DDR DDRB        
#define LED_PORT PORTB 

#define LED0_PIN 1                               
[...]
\end{minted}
\end{mdframed}
Des Weiteren wird die maximale Anzahl an LEDs pro Streifen definiert, so dass sich die Anwendung auch einfach auf ein anderes Projekt übertragen lässt, in welchem 
LED-Streifen einer anderen Länge verwendet werden. Außerdem werden die elementaren Funktionen \texttt{bit\_read}, \texttt{bit\_clear} und \texttt{bit\_set} benötigt, welche über 
Makros definiert werden, die dem Mikrocontroller-Skript zu entnehmen waren.
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
#define NMAX 32
#define bit_read(x,n) ((x) >> (n)) &1
#define bit_clear(x,n) ((x) &= ~(1<<n))
#define bit_set(x,n) ((x) |= (1<<(n)))
\end{minted}
\end{mdframed}

\subsubsection{Arrays für LED-Informationen}
Um nun die LED-Leisten anzusprechen wird für jede anzusprechende Leiste ein Array benötigt. Das Array speichert die Werte die eine Leiste ausgeben soll, indem es nacheinander die RGB
Werte der einzelnen LEDs der Leiste speichert. So sind z.B. die ersten drei Werte des Arrays einer Leiste die RGB Werte der ersten LED. Dabei werden die RGB Werte als Integers 
gespeichert und haben eine Spanne von 0 bis 255. Wird ein RGB Wert auf 0 gesetzt, leuchtet die entsprechende Farbe bei dieser LED nicht und eine Mischung der übrigen beiden 
ergibt sich. Wird ein RGB Wert auf 255 gesetzt, leuchtet diese Farbe in voller Stärke. Es werden 3 dieser Arrays benötigt, da das Projekt 3 LED-Streifen besitzt.
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
// array containing LED information (3 values for each LED)
unsigned int stripe0_array[(NMAX)*3+1];
unsigned int stripe1_array[(NMAX)*3+1];
unsigned int stripe2_array[(NMAX)*3+1];
\end{minted}
\end{mdframed}

Um diese Arrays zu befüllen, wird die Funktion \texttt{set\_pixel(...)} genutzt:
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
void set_pixel(unsigned char _R, unsigned char _G, unsigned char _B,
			unsigned char _N, int _stripe_pin) {
	switch (_stripe_pin) {
		case 1: {
			stripe0_array[(_N-1)*3] = _G * brightness;
			stripe0_array[(_N-1)*3+1] = _R * brightness;
			stripe0_array[(_N-1)*3+2] = _B * brightness;
			break;
		};
		[...]
	}
}
\end{minted}
\end{mdframed}

In dieser Funktion wird jeder RGB Wert, die Stelle der LED auf dem Streifen und der anzusprechende Streifen übergeben. Die Stelle der LED wird mit 3 
multipliziert, da jede LED wie bereits erwähnt 3 Werte besitzt. 

\pagebreak
An dieser Stelle ist zu sehen, dass zuerst der \dq grün\dq -Wert übergeben wird, dann die \dq rot\dq - und \dq blau\dq -Werte. Das ist darauf zurückzuführen, dass die LED-Leiste nicht das RGB-System verwendet, sondern das GRB-System.
Die Funktion \texttt{set\_rgb\_pixel(...)} ist eine Erweiterung dieser Funktion, bei der statt der einzelnen RGB Werte ein struct übergeben wird, welches diese Werte beinhaltet. So können 
viele Farben einfacher erstellt und besser wiederverwendet werden. 
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
struct RGB {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};
\end{minted}
\end{mdframed}
Es existiert eine \texttt{reset\_stripe\_array()} - Funktion, welche sämtliche Werte der RGB Arrays auf 0 setzt.

\subsubsection{Send-Funktionen}
Nun existiert ein sinnvolles Speicherformat für die Daten im Programm, allerdings müssen diese auch noch an die Leiste gesendet werden können.\\ Dafür zuständig ist die Funktion \texttt{send\_stripe\_array(int \_pin)}. Da ein Array jedoch nicht direkt gesendet werden kann, wird ein Format benötigt, welches in der Lage ist einzelne Bits zu übertragen. Dafür zuständig sind die \texttt{send\_bit(int \_delay)} - Funktionen. Für jeden LED-Streifen wird eine einzelne Funktion benötigt, da der Port und der Pin bereits in der Funktion festgelegt sind. Beispielhaft wird nun die Funktion für den ersten Streifen erläutert:
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
void send_bit1(int _delay) {
	if (_delay == 1) {
		bit_set(LED_PORT, LED0_PIN);
		delay_ns(T1H);
		bit_clear(LED_PORT, LED0_PIN);
	} else if (_delay == 0) {
		bit_set(LED_PORT, LED0_PIN);
		delay_ns(T0H);
		bit_clear(LED_PORT, LED0_PIN);
	}
}
\end{minted}
\end{mdframed}

\texttt{T1H} (Z.4) und \texttt{T0H} (Z.8) sind delays welche aus dem Datenblatt der LED übernommen worden sind. \texttt{T1H} ist das delay zum Senden einer logischen 1 (0.7us $\pm$ 150ns) und \texttt{T0H} für eine logische 0 (0.35us $\pm$ 150ns).

In beiden Fällen wird der enstprechende Pin der LED-Leiste zuerst auf 1 gesetzt und dann wieder auf 0. Der Unterschied besteht lediglich in der Länge der Verzögerung zwischen diesen Aktionen. Wird der Pin lange genug auf 1 gesetzt (>500 ns), dann registriert die LED-Leiste dies als eine 1, wird der Pin jedoch nur kurz aktiviert, registriert die Leiste dies als eine 0.

\pagebreak
Da es nun möglich ist Daten auf die Leiste zu transferieren, kann die bereits erwähnte\\ \texttt{send\_stripe\_array(int \_pin)} - Funktion dies nutzen. Wie zuvor, wird beispielhaft der 
Vorgang für den ersten Streifen erläutert werden, die anderen sind als analog zu betrachten.
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
void send_stripe_array(int _pin) {
	switch (_pin) {
		case 1: {
			for (int i = 0; i < NMAX * 3; i++) {
				for (int j = 0; j < 8; j++) {
					send_bit1(bit_read(stripe0_array[i],(7-j)));
				}
			}
			break;
		};
		[...]
	}
}
\end{minted}
\end{mdframed}

Je nach Wahl der Variable \texttt{\_pin} wird einer der 3 LED-Streifen ausgewählt. In der äußeren for-Schleife wird über das gesamte RGB Array iteriert, welches dem gewählten Streifen 
entspricht. Die innere for-Schleife wird benötigt, da nur binäre Werte übertragbar sind. Da der höchste RGB Wert (255) 8 Bits zur Darstellung benötigt, muss die Schleife 8 
mal durchlaufen. Es folgt also, dass die RGB Werte in Binärzahlen umgerechnet werden müssen. Dies geschieht in der \texttt{bit\_read} - Funktion. Deren Übergabeparameter sind
das benötigte RGB Array, den RGB Wert welcher übertragen werden soll und die entsprechende Stelle des RGB Bytes, welche zu übertragen ist. Die RGB Bits werden nacheinander an die Leiste übergeben. Dabei wird die Reihenfolge der Bits in den Bytes umgekehrt. Dies wird benötigt, da die Leiste die Bits eines Bytes in anderer Reihenfolge erwartet, als der MC sie intern 
speichert.

Die Kommunikation zwischen dem MC und den LED-Leisten ist nun funktionsfähig, jedoch sollte eine Möglichkeit existieren, die Helligkeit der LEDs global abzuändern. Dazu wurde die 
Variable
\begin{mdframed}[backgroundcolor=arsenic]
\begin{minted}[obeytabs=true,tabsize=2,xleftmargin=20pt,linenos]{c}
float brightness;
\end{minted}
\end{mdframed}
initialisiert. Diese nimmt einen Wert zwischen 0 und 1 an. Jeder RGB Wert, der in eines der Arrays geschrieben wird, wird mit der Brightness-Variable multipliziert. Ist diese also z.B. 0.8, 
leuchtet jede LED mit maximal 80\% ihrer Stärke. Ist die brightness auf 0 gesetzt, leuchtet keine einzige LED. Die brightness gilt für alle Streifen statt für einzelne.
Nun, da die Kommunikation zwischen MC und LED-Leisten implementiert ist, ist es sinnvoll die Implementierung der Buttons zu erläutern, da diese sehr ähnlich zu der der LED-Leisten ist.