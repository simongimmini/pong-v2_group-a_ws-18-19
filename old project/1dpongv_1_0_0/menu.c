/*
 * menu.c
 *
 * Created: 10.09.2015 16:18:37
 *  Author: freeskierlap
 */ 

 /* hier soll ein file entstehen das ein menu generiert.
 *	Mit einmal dr�cken wechselt man den Punkt und mit halten w�hlt man ihn aus
 */

 
#include "menu.h"


void menu(){
	menumax = 5;	//anzahl der menupunkte
	
	
	while(quit == 0){
			
		switch(menuchoice){
			default: {
				quit=0;
				menuchoice=1;
			}			
			case 1: {		
				if(menupunktauswahl(1, &gamechoice))break;
			}
			case 2: {
				if(menupunktauswahl(2, &gamechoice))break;
			}
			case 3: {
				if(menupunktauswahl(3, &gamechoice))break;
			}
			/*
			case 4: {
				if(menupunktauswahl(4, &gamechoice))break;

			}
						*/
			case 4: {
				if(menupunktauswahl(4, &gamechoice))break;
			}
		}

	}
	quit = 0;			//this way the next time the menu is called it won't just quit again.
}

void menuBrightness(){
	menumax = 4;	//anzahl der menupunkte
	quit = 0;

	while(quit == 0){
			
		switch(menuchoice){
			default: {
				quit=0;
				menuchoice=1;
			}			
			case 1: {		
				if(menupunktauswahlbrightness(1))break;
			}
			case 2: {
				if(menupunktauswahlbrightness(2))break;
			}
			case 3: {
				if(menupunktauswahlbrightness(3))break;
			}

		}

	}
	quit = 0;			//this way the next time the menu is called it won't just quit again.
}


void showmenulight(int i){
	ResetPixelArray();
	SetPixelArray(0,255,255,MENU_START_POINT+(i * MENU_INCREMENT));
	SendPixelArray();
}

void flashselect(int i){
	for(int j=0;j<3;j++){
		ResetPixelArray();
		SetPixelArray(0,255,255,MENU_START_POINT+(i * MENU_INCREMENT));			//this sets the spacing of the menupoints
		SendPixelArray();
		_delay_ms(250);
		ResetPixelArray();
		SendPixelArray();
		_delay_ms(250);
	}
}


int menupunktauswahl(int nummer, int *auswahlvariable){
	buttonhalten=0;
	showmenulight(nummer);
	do{
		button1check(&menubutton1);				
		button2check(&menubutton2);
	} while(menubutton1==0 && menubutton2==0);	//h�lt die schleife bis ein button gedr�ckt wird
	do{											//l�uft 10 mal, wenn einer der button so lange gehalten wird
		buttonhalten=buttonhalten+1;
		_delay_ms(100);
		button1check(&menubutton1);
		button2check(&menubutton2);
	} while((menubutton1==1 || menubutton2)&& buttonhalten <10);
	if (buttonhalten==10){						//wenn der button 1s gehalten wurde:
		*auswahlvariable = nummer;				//beendet switch schleife und setzt
		quit = 1;
		flashselect(nummer);
		return(1);									//die auswahl auf 1
	}
	quit = 0;
	if (menuchoice<menumax)menuchoice++;
	if (menuchoice==menumax)menuchoice=1;
	return(0);
}

int menupunktauswahlbrightness(int nummer){
	switch(nummer){							//this sets the brightness according to the menupoint
		case 1: SetBrightness(1);break;
		case 2: SetBrightness(0.5);break;
		case 3: SetBrightness(0.1);break;
	}
	buttonhalten=0;
	for(int i=1;i<=numberpix();i++){ 		//numberpix returns #LEDS
		SetPixelArray(255,255,255,i);		//since brightness is already set, this represents the actual maximum brightness
	}		
	SendPixelArray();
	do{
		button1check(&menubutton1);				
		button2check(&menubutton2);
	} while(menubutton1==0 && menubutton2==0);	//h�lt die schleife bis ein button gedr�ckt wird
	do{											//l�uft 10 mal, wenn einer der button so lange gehalten wird
		buttonhalten=buttonhalten+1;
		_delay_ms(100);
		button1check(&menubutton1);
		button2check(&menubutton2);
	} while((menubutton1==1 || menubutton2)&& buttonhalten <10);
	if (buttonhalten==10){					//wenn der button 1s gehalten wurde:			
		quit = 1;							//beendet switch schleife und setzt
		for (int i = 0; i<=255;i++){		//slowly fades out the light
			for(int j = 0; j <= numberpix(); j++){
				SetPixelArray(255-i,255-i,255-i,j);
			}
			SendPixelArray();
		}
		return(1);									//die auswahl auf 1
	}
	quit = 0;
	if (menuchoice<menumax)menuchoice++;
	if (menuchoice==menumax)menuchoice=1;
	return(0);
}


