/*
 * 
 *
 * Created: 02.08.2015 12:16:58
 *  Author: Julius und Jascha
 */ 


//	call StripeInit(); at least once in the beginning. This sets the desired pin to output and resets everything.
//	if more than 1 led stripe is used one needs to rewrite this file
//	



#ifndef LEDCOMMUNICATION_H
#define LEDCOMMUNICATION_H


#define F_CPU 16000000			//define properly
#include <avr/io.h>	
#include <util/delay.h>

//these definitions are only needed if stripeinit does not work then the other sendbit() function should be used
#define PIXEL_PORT PORTB		//defines the port
#define PIXEL_DDR DDRB			//defines the data register
#define PIXEL_BIT 5				//defines the pin


#define NMAX 24				//defines the number of pix

/* defines the bit functions and the singal lengts */
#define T0H 200					//times must be given in ns. This must be shorter than 500 ns including the cycles needed to set the pin (about 6)
#define T0L 0
#define T1H 500					//must be longer than 500 ns including the cycles needed to set the pin (about 6)
#define T1L 0
#define TRES 60000				//must be at least 5 us. On our CPU its not needed because its "slow enough" SendPixelArray() may need some tweaking on faster CPU's
#define delay_ns(ns) (__builtin_avr_delay_cycles((double)F_CPU/1000000000*ns))
#define bitRead(x,n) ((x) >> (n)) &1		//typical macros
#define bitClear(p,n) ((p) &= (~(1) << (n)))
#define bitSet(x,n) ((x) |= (1<<(n)))
	

unsigned int pixelarray[(NMAX)*3+1];		//array containing led information. 3 values for each pixel
float brightness;						//float that sets the brightness. 1 is maximum. 0.2 would be 1/5 maximum brightness



void sendbit(int a);							//sends one bit through the choosen pin
void SetPixelArray(unsigned char R, unsigned char G, unsigned char B, unsigned char N);  //sets the 3 values of the N-th pixel in the Array
void ResetPixelArray();							//sets every entry in the array to zero
int numberpix();								//returns the number of pixels
void SendPixelArray();							//sends the pixel array to the stripe
void StripeInit(char pixel_ddr, int pixel_pin,int test);	//first parameter specifies the DDR i. e. B C or D; second the pin; third enables stripetest = 1								
void SetBrightness(float value);				//sets the maximum brightness; "255/value" where 255 is maximum




#endif

