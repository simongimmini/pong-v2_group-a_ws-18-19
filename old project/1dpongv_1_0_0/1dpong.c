/*
 * _1dpong.c
 *
 * Created: 02.08.2015 21:08:13
 *  Author: freeskierlap
 */
#define NMAX 24		//old project: 60
#define X_CORDMAX 24	//pls use a multiple of 2
#define F_CPU 16000000
//only needed if strpeinit and buttonint don't work

#define PIXEL_PORT PORTB		//defines the port
#define PIXEL_DDR DDRB			//defines the data register
#define PIXEL_BIT 5					//defines the pin
// definitions for buttons
#define BUTTON_DDR DDRC
#define BUTTON_PORT PORTC
#define BUTTON_1_PIN 0
#define BUTTON_2_PIN 1


#include <avr/io.h>
#include <util/delay.h>
#include "ledcommunication.h"
#include "buttoncontrol.h"
#include "Gamecontrol.h"
#include "menu.h"




int main(void)
{
	//StripeInit('B',5,1);			//stripe iniated on Data Register B and Pin 5, also stripe test is true (last parameter)
	//ButtonInit('C',0,1);			//2 buttons iniated on Data Register C and Port C, at pin 0 and pin 1


	/* 	debugging code */
	DDRC=0x00;				// DDRC direction to input
	bitSet(PORTC,0);		// Pin 0 and Pin 1 on Port C to listen
    bitSet(PORTC,1);
	DDRB |= (1<<5);			// Pin 5 on On DDRB to output for LED
	brightness = 1;
	//test flashing to see wheather the code starts or not
	while(1){
	ResetPixelArray();
	SetPixelArray(100,0,0,12);
	SendPixelArray();
	_delay_ms(1000);
	SetPixelArray(0,100,0,12);
	SendPixelArray();
	_delay_ms(1000);
	}
	/*	debugging code*/

	while(1){

		menu();					//startet das Menu. Die Auswahl wird in "gamechoice" gespeichert


		//actual gamestart depending on gamechoice
		switch(gamechoice){
			case 1:{							//ball gets faster after every reflection
				GameInit();						//sets basic parameters and shows a animation
				GameStart(&x_cord, 75);						//puts the ball1 in motion
				while(1){
					showposition(&x_cord,&ballColor,&direction,&speed);		//displays the current position of the ball
					GameMove(&x_cord,&direction);			//moves the ball1 depneding on position, direction and buttonstate
					checkwin(&x_cord);						//checks whether ball1 ran into the end of the stripe or
															//one button was pressed too early if so gives the winner a point

					if (player1points == 5){				//if one player reaches 5 points the game ends
						winanimation(1);
						_delay_ms(5000);
						break;
					}
					if (player2points == 5){
						winanimation(2);
						_delay_ms(5000);
						break;
					}
				}
				break;
			}
			case 2:{							//now the speed of the ball depends on the reaction area (parabolic)
				GameInit();
				GameStart(&x_cord, 70);
				while(1){
					showposition(&x_cord,&ballColor,&direction,&speed);
					GameMove2(&x_cord,&direction);
					checkwin(&x_cord);
					if (player1points == 5){
						winanimation(1);
						_delay_ms(5000);
						break;
					}
					if (player2points == 5){
						winanimation(2);
						_delay_ms(5000);
						break;
					}
				}
				break;
			}
			case 3:{							//now the speed of the ball depends on the reaction area (linear rising to end)
				GameInit();
				GameStart(&x_cord, 70);
				while(1){
					showposition(&x_cord,&ballColor,&direction,&speed);
					Gamemove3(&x_cord,&direction);
					checkwin(&x_cord);
					if (player1points == 5){
						winanimation(1);
						_delay_ms(5000);
						break;
					}
					if (player2points == 5){
						winanimation(2);
						_delay_ms(5000);
						break;
					}
				}
				break;
			}
			/*
			case 4:{							//single player game
				GameInitSingle();				//wenn 10 punkte geschafft sind erscheit zweiter ball
				GameStartSingle(&x_cord);		//wäre cool
				while(1){
					showposition(&x_cord,&ballColor,&direction,&speed);
					GameMoveSingle(&x_cord,&direction);
					if(CheckLoseSingle(&x_cord)==1)break;
				}
				break;
			}
			 */
			case 4:{							//brightness settings
					SetPixelArray(100,0,0,1);
					SendPixelArray();
					menuBrightness();
				break;
			}
		}
	ResetPixelArray();
	SetPixelArray(100,0,0,20);
	SendPixelArray();
	_delay_ms(1000);
	SetPixelArray(0,100,0,20);
	SendPixelArray();
	_delay_ms(1000);
	}
}
