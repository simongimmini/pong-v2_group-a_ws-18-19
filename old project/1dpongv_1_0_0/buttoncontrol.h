/*
 * buttoncontrol.h
 *
 * Created: 02.08.2015 21:11:37
 *  Author: freeskierlap
 */ 
 /* NOOOOOOOOOOOOOTTTTTTTTTTTTEEEEEEEEEEE
  * 
  * These commands are needed in main!!!!
  * 	DDRC=0x00;
	bitSet(PORTC,0);
    bitSet(PORTC,1);  
    * to set the ports the right way...
    */
 
 
 
#include <avr/io.h>

#ifndef BUTTONCONTROL_H
#define BUTTONCONTROL_H

#define bitRead(x,n) ((x) >> (n)) &1
#define bitClear(p,n) ((p) &= (~(1) << (n)))
#define bitSet(x,n) ((x) |= (1<<(n)))
#define BUTTON_DDR DDRC	/*connect the buttons to the same register!!!*/
#define BUTTON_PORT PORTC
#define BUTTON_2_PORT PORTC
#define BUTTON_1_PIN 0
#define BUTTON_2_PIN 1



void button1check(int *state);					/*returns the value of the bit pin*/
void button2check(int *state);
void ButtonInit(char port,char pin_1,char pin_2);	//sets the port, data register and pins the right way; look into c file for more information






#endif /* BUTTONCONTROL_H */
