/*
 * Gamecontrol.h
 *
 * Created: 02.08.2015 21:26:37
 *  Author: freeskierlap
 */


#ifndef GAMECONTROL_H_
#define GAMECONTROL_H_

#define X_CORDMAX 24
#define INITIAL_SPEED 80
#define NMAX 24


#include <avr/io.h>
#include <util/delay.h>
#include "buttoncontrol.h"
#include "ledcommunication.h"
#include "menu.h"


/*Variablen*/
int x_cord;
int direction;
char ballColor;						//r g b or w
int speed;
int FlowColor[2];						//255 different Colors r to g to b

int x_cord2;		//2nd ball
int direction2;
char ballColor2;
int speed2;


int button1state;
int button2state;
int player1points;
int player2points;


//first game						//ball gets faster after every reflection
void GameInit();			
void GameStart(int *position, int initspeed); //wants to know the initial speed, depends on the game
void GameMove(int *cord,int *dir);
void checkwin(int *cord);			/*returns 1 for player1 and 2 for player2, 0 for none*/
void changecolor(char *ball);
void showscore();					//shows the score of each player with blue dots


//secondgame						//now the speed of the ball depends on the reaction area
void GameMove2(int *cord,int *dir);


//thirdgame							same as second only does speed depend on how late you hit the ball
void Gamemove3(int *cord,int *dir);

//4th game SinglePlayer
void GameInitSingle();				// sets basic parameters
void GameMoveSingle(int *cord, int *dir);		//works a little different than 2 player mode; look c file
int CheckLoseSingle(int *cord);
void ShowScoreSingle();							//displays score in binary form. Every succesful hit counts as 1 point
void GameStartSingle(int *position);

//other functions
void showposition(int *cord,char *color,int *dir,int *speedpointer);		//simply shows the position of the ball. Was a little more complicated in the beginning
void RedFlash(char cord_lose);
void winanimation(int who);			//if who=1 player 1 gets the animation if who=2 player 2 gets the animation
void winanimationEnd(char who);		//winanimation for the end of the whole game
void init();						//fancy start animation...
//void ShowPosMatrixC(int *cord,int *color[2]);			//also shows the position of the ball but with changing color

//******************function bodys**********************



#endif /* GAMECONTROL_H_ */
