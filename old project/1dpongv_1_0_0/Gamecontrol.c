/*
 * Gamecontrol.c
 *
 * Created: 02.08.2015 21:26:37
 *  Author: freeskierlap
 */
 
 
 #include "Gamecontrol.h"
 
 static char Fail_Coord;
 
 //******************first game**************************
void GameInit(){
	x_cord = X_CORDMAX/2;
	direction = 1;
	ballColor = 'b';
	speed = 80;
	
	x_cord2 = X_CORDMAX/2;
	direction2 = 1;
	ballColor2 = 'r';
	speed2 = 50;
	//score is set to minus one. This way the first ball doesn't count. Because it always goes in the same direction...
	player1points = -1;								//if you don't restart the controller between games 
	player2points = -1;								//score needs to be reset 
	init();											//some start animation
}

void GameStart(int *position, int initspeed){
	showscore();
	_delay_ms(2000);										//shows the score for 2 s
	speed = initspeed;
	if(*position==X_CORDMAX/2){								//flashes the ball for 5 s then starts it
		for(int i=0;i<5;i++){
			ballColor = 'w';				
			showposition(position,&ballColor,&direction,&speed);
			_delay_ms(500);
			ResetPixelArray();
			SendPixelArray();
			_delay_ms(500);
		}
	}
	button1check(&button1state);
	button2check(&button2state);
	if(*position==X_CORDMAX/12){								//rests the ball at the last set location until the
		while(button2state==0){									//the player presses his button
			ballColor = 'g';
			showposition(position,&ballColor,&direction,&speed);				//the location is set in checkwin()
			button2check(&button2state);
		}
	}
	button1check(&button1state);
	button2check(&button2state);
	if(*position==X_CORDMAX-(X_CORDMAX/12)){
		while(button1state==0){
			ballColor = 'g';
			showposition(position,&ballColor,&direction,&speed);
			button1check(&button1state);
		}
	}
}

void GameMove(int *cord,int *dir){
		button1check(&button1state);
		button2check(&button2state);
		Fail_Coord = *cord;										//saves ballposition
		/*moving the ball when no buttons are pressed*/
		if (direction==1 && *cord<X_CORDMAX+1){
			*cord = *cord+1;
		}
		if (direction==-1 && *cord>0){
			*cord = *cord-1;
		}
		/*changes direction of ball when buttons are pressed correctly*/
		if (*dir==-1 && *cord<(X_CORDMAX/6) && button2state ==1){
			*dir=1;
			changecolor(&ballColor);
			speed++;
		}
		if (direction==1 && *cord>((X_CORDMAX-(X_CORDMAX/6))+1) && button1state==1){
			*dir=-1;
			changecolor(&ballColor);
			speed++;
		}
		/*puts the ball to either end if button is pressed to early*/
		if (*dir==-1 && *cord>=(X_CORDMAX/6) && button2state==1){							
			x_cord=0;
		}
		if (*dir==1 && *cord<=((X_CORDMAX-(X_CORDMAX/6))+1) && button1state==1){
			x_cord=X_CORDMAX+1;
		}
		for (int j=100;j>speed;j--){
		_delay_ms(1);
		}
}

void checkwin(int *cord){				//checks wheather the ball is at either end of the stripe
	if(*cord==0){
		if(player1points==-1)player1points=0;	//this way the first ball is "um die Angabe"
		player2points++;
		*cord=X_CORDMAX/12;				//places the ball in the middle of the reaction zone
		winanimation(2);
		showscore();					
		direction = 1;
		if(player2points<5 && gamechoice == 1 )GameStart(&x_cord, 70);		//starts GameStart depending on gamechoice (initial speed!)
		if(player2points<5 && (gamechoice == 2 || gamechoice == 3))GameStart(&x_cord, 90);
	}
	if(*cord==X_CORDMAX+1){
		if(player2points==-1)player2points=0;
		player1points++;
		*cord=X_CORDMAX-(X_CORDMAX/12);
		winanimation(1);
		showscore();
		direction = -1;
		if(player1points<5 && gamechoice == 1 )GameStart(&x_cord, 70);
		if(player1points<5 && (gamechoice == 2 || gamechoice == 3))GameStart(&x_cord, 90);
	}
}

void changecolor(char *ball){			//changes color of ball in an easy way
	switch(*ball){
		case 'r': *ball = 'g';
					break;
		case 'g': *ball = 'b';
					break;
		case 'b': *ball = 'r';
					break;
		case 'w': *ball = 'r';
					break;
	}
}

void showscore(){
	ResetPixelArray();
	for (int i=1;i<=player1points;i++)
	{
		SetPixelArray(0,0,255,(2*i)-1);
	}
	for (int i=1;i<=player2points;i++)
	{
		SetPixelArray(0,0,255,NMAX-((2*i)-2));
	}
	SendPixelArray();
	_delay_ms(2000);
}

void winanimation(int who){
	ResetPixelArray();
	if(who==1){
		if(Fail_Coord < X_CORDMAX)RedFlash(Fail_Coord);
		else RedFlash(X_CORDMAX);
		for (int n=1;n<NMAX+1;n++){
			if (n<=NMAX/2){
				SetPixelArray(255,0,0,n);				//winner side green
			}
			if (n>NMAX/2){
				SetPixelArray(0,255,0,n);				//loser side red
			}
		}
	}
	if (who==2){										//same as who==1
		if(Fail_Coord > 0)RedFlash(Fail_Coord);			//flash at failed position
		else RedFlash(1);					//flash at end
		for (int n=1;n<NMAX+1;n++){
			if (n>=NMAX/2){
				SetPixelArray(255,0,0,n);
			}
			if (n<NMAX/2){
				SetPixelArray(0,255,0,n);
			}
		}
	}
	SendPixelArray();
	_delay_ms(2000);
}

void init(){
	/*	soll die LEDS nacheinander zur Mitte hin anmachen 
	* 	und dann von ausen wieder aus.
	* 	Von beiden seiten.
	*	Das ganze drei mal mit verschiedenen farben
	*/
	
	for (int i=1;(i<NMAX/2);i++){				 
		SetPixelArray(255-60+i,0,255-60+i,i);	//leds gehen von au�en nach innen an
		SetPixelArray(0,255-60+i,255-60+i,61-i);
		SendPixelArray();
		_delay_ms(10);
	}
	for (int i=1;i<(NMAX/2);i++){				 
		SetPixelArray(0,0,0,31-i);				//leds gehen von au�en nach innen aus
		SetPixelArray(0,0,0,30+i);
		SendPixelArray();
		_delay_ms(10);
	}
	//zweites mal jedoch andere farben
	for (int i=1;(i<NMAX/2);i++){				 
		SetPixelArray(255-60+i,255-60+i,120,i);	//leds gehen von au�en nach innen an
		SetPixelArray(120,255-60+i,255-60+i,61-i);
		SendPixelArray();
		_delay_ms(10);
	}
	for (int i=1;i<(NMAX/2);i++){				 
		SetPixelArray(0,0,0,31-i);				//leds gehen von au�en nach innen aus
		SetPixelArray(0,0,0,30+i);
		SendPixelArray();
		_delay_ms(10);
	}
		//drittes mal in gr�n
	for (int i=1;(i<NMAX/2);i++){				 
		SetPixelArray(255-60+i,0,0,i);	//leds gehen von au�en nach innen an
		SetPixelArray(255-60+i,0,0,61-i);
		SendPixelArray();
		_delay_ms(10);
	}
	for (int i=1;i<(NMAX/2);i++){				 
		SetPixelArray(0,0,0,31-i);				//leds gehen von au�en nach innen aus
		SetPixelArray(0,0,0,30+i);
		SendPixelArray();
		_delay_ms(10);
	}
}


//******************second game**************************

void GameMove2(int *cord,int *dir){
	button1check(&button1state);
	button2check(&button2state);
	Fail_Coord = *cord;
	/*moving the ball when no buttons are pressed*/
	if (direction==1 && *cord<X_CORDMAX+1){
		*cord = *cord+1;
	}
	if (direction==-1 && *cord>0){
		*cord = *cord-1;
	}
	/*	changes direction of ball when buttons are pressed correctly 
	*	and sets speed depending on "hit" area, Middle is best.
	*/
	if (*dir==-1 && *cord<(X_CORDMAX/6) && button2state ==1){
		*dir=1;
		changecolor(&ballColor);
		speed=( -2* (*cord)* (*cord) )+ (20* *cord) + 50; // sets the speed according to hitpoint
	}
	if (direction==1 && *cord>((X_CORDMAX-(X_CORDMAX/6))+1) && button1state==1){
		*dir=-1;
		changecolor(&ballColor);
		speed=( -2* (60-*cord)* (60-*cord) )+ (20* (60-*cord)) + 50; // sets the speed according to hitpoint
	}
		/*puts the ball to either end if button is pressed to early*/
	if (*dir==-1 && *cord>=(X_CORDMAX/6) && button2state==1){
		x_cord=0;
	}
	if (*dir==1 && *cord<=((X_CORDMAX-(X_CORDMAX/6))+1) && button1state==1){
		x_cord=X_CORDMAX+1;
	}
	for (int j=100;j>speed;j--){			//controls the speed
	_delay_ms(1);
	}		
}

//******************third game*************************

void Gamemove3(int *cord,int *dir){
	button1check(&button1state);
	button2check(&button2state);
	Fail_Coord = *cord;
	/*moving the ball when no buttons are pressed*/
	
	if (direction==1 && *cord<X_CORDMAX+1){
		*cord = *cord+1;
	}
	if (direction==-1 && *cord>0){
		*cord = *cord-1;
	}
	/*	changes direction of ball when buttons are pressed correctly 
	*	and sets speed depending on "hit" area, end is best.
	*/
	
	if (*dir==-1 && *cord<(X_CORDMAX/6) && button2state ==1){
		*dir=1;
		changecolor(&ballColor);
		speed=100-(*cord * 4); // sets the speed according to hitpoint

		
	}
	if (direction==1 && *cord>((X_CORDMAX-(X_CORDMAX/6))+1) && button1state==1){
		*dir=-1;
		changecolor(&ballColor);
		speed=100-((60- *cord) * 4); // sets the speed according to hitpoint

	}
		/*puts the ball to either end if button is pressed to early*/
	
	if (*dir==-1 && *cord>=(X_CORDMAX/6) && button2state==1){
		x_cord=0;
	}
	if (*dir==1 && *cord<=((X_CORDMAX-(X_CORDMAX/6))+1) && button1state==1){
		x_cord=X_CORDMAX+1;
	}
	
	for (int j=100;j>speed;j--){			//controls the speed
	_delay_ms(1);
	}
		
}


//******************4th game***************************

int maxheight;				//maximum height that can be achieved with this hit

void GameInitSingle(){
	player1points = 0;
	x_cord = 5;
	direction = 1;
	speed = 5;
	maxheight = 30;
}

void GameMoveSingle(int *cord,int *dir){
	
	button1check(&button1state);
	if (button1state == 0 && *cord > 10 && *cord < maxheight && *dir == 1){
		*cord = *cord + 1;
		}
	if (button1state == 0 && *cord > 10 && *dir == -1){
		*cord = *cord - 1;
	}
	if (button1state == 0 && *cord == maxheight && *dir == 1){
		*dir = -1 ;
	}
	if (button1state == 1 && *cord > 10){
		*cord = 0;
	}
	if (button1state == 1 && *cord <= 10){
		*dir = 1;
		maxheight = 70 - (10 * *cord);
		player1points++;
	}
	speed = 60 - (((maxheight - 10) / 60.0)* *cord);
	for (int i = 60; i > speed; i++){
		_delay_ms(1);
	}
}

void ShowScoreSingle(){			//represents the score in bit form
	ResetPixelArray();
	for (int k = 0; k < 7;k++){
		if((player1points >> k) & 1)SetPixelArray(255,255,255,(k+1)*2);
	}
	SendPixelArray();
	_delay_ms(1000);
	while(button1state==0){				//h�lt den Score bis etwas gedr�ckt wird
		button1check(&button1state);
	}
}

int CheckLoseSingle(int *cord){
	if (*cord == 0){
		for (int i=0;i<2;i++){							//red flash at loser side
			SetPixelArray(0,255,0,1);
			SendPixelArray();
			_delay_ms(500);
			SetPixelArray(0,0,0,1);
			SendPixelArray();
			_delay_ms(500);
		}
		ShowScoreSingle();
		return(1);
	}
	else return(0);
}

void GameStartSingle(int *position){
	while(button1state==0){
		ballColor = 'g';
		showposition(position,&ballColor,&direction,&speed);
		button1check(&button1state);
	}
}


//other functions


void showposition(int *cord,char *color,int *dir,int *speedpointer){
	ResetPixelArray();
	switch(*color){
		case 'r': {
			SetPixelArray(0,255,0,*cord);
			SendPixelArray();
			break;
		}
		case 'g': {
			SetPixelArray(255,0,0,*cord);
			SendPixelArray();
			break;
		}
		case 'b': {
			SetPixelArray(0,0,255,*cord);
			SendPixelArray();
			break;
		}
		case 'w': {
			SetPixelArray(255,255,255,*cord);
			SendPixelArray();
			break;
		}
	}
}

void RedFlash(char cord_lose){
	ResetPixelArray();
	for(int i=0;i<10;i++){
		SetPixelArray(0,255,0,cord_lose);
		SendPixelArray();
		_delay_ms(125);
		ResetPixelArray();
		SendPixelArray();
	}
}
/*
void winanimationEnd(char who){
	ResetPixelArray();
	if(who==1){
		for(char j=0;j<5;j++){
			for(char i = 0;i<60/2;i++){			//bounces 2 balls; green at winner side and red at loser side
				SetPixelArray(255,0,j*51,i+1);
				SetPixelArray(0,0,0,i);
				
				SetPixelArray(0,255,j*51,60-i);
				SetPixelArray(0,0,0,60+1-i);
				
				SendPixelArray();
			}
			for(char i = 0;i<60/2;i++){
				SetPixelArray(255,0,j*51,(60/2)-i);
				SetPixelArray(0,0,0,(60/2)-i+1);
				
				SetPixelArray(0,255,j*51,(60/2)+i+1);
				SetPixelArray(0,0,0,(60/2)+i);
				
				SendPixelArray();
			}
		}
	}
	if(who==2){
		for(char j=0;j<5;j++){
			for(char i = 0;i<60/2;i++){			//bounces 2 balls; green at winner side and red at loser side
				SetPixelArray(0,255,j*51,i+1);
				SetPixelArray(0,0,0,i);
				
				SetPixelArray(255,0,j*51,60-i);
				SetPixelArray(0,0,0,60+1-i);
				
				SendPixelArray();
			}
			for(char i = 0;i<60/2;i++){
				SetPixelArray(0,255,j*51,(60/2)-i);
				SetPixelArray(0,0,0,(60/2)-i+1);
				
				SetPixelArray(255,0,j*51,(60/2)+i+1);
				SetPixelArray(0,0,0,(60/2)+i);
				
				SendPixelArray();
			}
		}
	}
}
*/

