/*
 * menu.h
 *
 * Created: 10.09.2015 16:18:37
 *  Author: freeskierlap
 */ 

 /* 
 *	Mit einmal dr�cken wechselt man den Punkt und mit halten w�hlt man ihn aus
 */

#ifndef MENU_H
#define MENU_H

//definitions for the positions of the menupoints on the stripe
#define MENU_START_POINT	10
#define MENU_INCREMENT		6

#include <avr/io.h>
#include <util/delay.h>
#include "buttoncontrol.h"
#include "ledcommunication.h"


int menubutton1;
int menubutton2;
int gamechoice;
int brightnesschoice;
int quit;
int menuchoice;
int buttonhalten;
int menumax;	

//prototypes

void menu();						//hauptfunktion die je nach auswahl ein spiel ausf�hrt
void menuBrightness();				//works like menu() just sets other variables and displays the choice different
void showmenulight(int i);			//den punkt zum leuchten bringen der gerade ausgew�hlt ist
int menupunktauswahl(int nummer,int *auswahlvariable);	//reglt den menupunkt und setzt die Auswahlvariable auf nummer
int menupunktauswahlbrightness(int nummer); //...
void flashselect(int i);			//flashes the menupoint



#endif /* MENU_H */
