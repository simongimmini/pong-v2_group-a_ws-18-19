/*
 * ledcommunication.h
 *
 * Created: 27.09.2015 16:11:58
 *  Author: Julius und Jascha
 */ 


#include "ledcommunication.h"

static volatile uint8_t *pixel_port;
static int pixel_pin_st;


/* THIS IS THE OTHER sendbit() function. Use it if stripeinit won't work. stripinit() may have problems on slower CPU's. */
void sendbit(int a){
	if (a==1){
		bitSet(PIXEL_PORT,PIXEL_BIT);
		delay_ns(T1H);						//needs to be longer than 500ns including the ticks needed to clear and set the bit!!!
		bitClear(PIXEL_PORT,PIXEL_BIT);
		//delay_ns(T1L);					//must not exceed 5us. Commented out because our processor needs enough time anyway
	}
	if (a==0){
		bitSet(PIXEL_PORT,PIXEL_BIT);
		delay_ns(T0H);						//needs to be shorter than 500ns including the ticks needed to clear and set the bit!!!
		bitClear(PIXEL_PORT,PIXEL_BIT);
		//delay_ns(T0L);					//must not exceed 5us
	}
	
}

/*
void sendbit(int a){
	if (a==1){
		*pixel_port |= (1<<(pixel_pin_st));
		delay_ns(T1H);						//needs to be longer than 500ns including the ticks needed to clear and set the bit!!!
		*pixel_port &= (~(1) << (pixel_pin_st));
		//delay_ns(T1L);					//must not exceed 5us. Commented out because our processor needs enough time anyway
	}
	if (a==0){
		*pixel_port |= (1<<(pixel_pin_st));
		delay_ns(T0H);						//needs to be shorter than 500ns including the ticks needed to clear and set the bit!!!
		*pixel_port &= (~(1) << (pixel_pin_st));
		//delay_ns(T0L);					//must not exceed 5us
	}
}
*/

void SendPixelArray(){

	for (int i=0;i<NMAX*3;i++){					//goes through every entry of the pixel array
		for (int j=0; j<8;j++){					//sends the bits in reversed order
		sendbit(bitRead(pixelarray[i],(7-j)));	//gives the values from pixelarray to sendbit(int a) in reversed order!
		}
	}

	
}

void SetPixelArray(unsigned char R, unsigned char G, unsigned char B, unsigned char N){
	pixelarray[(N-1)*3] = R*brightness;
	pixelarray[(N-1)*3+1] = G*brightness;
	pixelarray[(N-1)*3+2] = B*brightness;
}

void ResetPixelArray(){
	for (int i=0;i<NMAX*3;i++){
		pixelarray[i] = 0;
	}
}



int numberpix(){
	return(NMAX);
}

void StripeInit(char pixel_ddr, int pixel_pin,int test){
	SetBrightness(1);
	pixel_pin_st = pixel_pin;
	switch(pixel_ddr){				//sets the PORT and DDR depending on pixel_ddr char. 
		case 'B': {
			DDRB |= (1<<pixel_pin);
			pixel_port = &PORTB;
		}
		case 'C': {
			DDRC |= (1<<pixel_pin);
			pixel_port = &PORTC;
		}
		case 'D': {
			DDRD |= (1<<pixel_pin);
			pixel_port = &PORTD;
		}
	}
	ResetPixelArray();						//inital testing of led stripe
	if (test ==1){
		for (int i = 1;i <= NMAX ; i++){
			SetPixelArray(0,255,0,i);
			SendPixelArray();
		}	
		for (int i = 1;i <= NMAX ; i++){
			SetPixelArray(255,0,0,i);
			SendPixelArray();
		}
		for (int i = 1;i <= NMAX ; i++){
			SetPixelArray(0,0,255,i);
			SendPixelArray();
		}
		for (int i = 1;i <= NMAX ; i++){
			SetPixelArray(0,0,0,i);
			SendPixelArray();
		}
	}
}

void SetBrightness(float value){
	brightness = value;
}

