/*
 * buttoncontrol.c
 *
 * Created: 02.08.2015 21:11:37
 *  Author: freeskierlap
 */


#include "buttoncontrol.h"

static volatile uint8_t *pin_register;
static char button1_pin;
static char button2_pin;


void button1check(int *state){
	if(bitRead(PINC,0)==1){
		*state=0;
	}
	else{
		*state=1;
	}

}

void button2check(int *state){
	if(bitRead(PINC,1)==1){
		*state=0;
	}
	else{
		*state=1;
	}
}


void ButtonInit(char port,char pin_1,char pin_2){
	button1_pin = pin_1;
	button2_pin = pin_2;
	switch(port){
		case 'B': {
			DDRB &= (~(1)<<pin_1);
			DDRB &= (~(1)<<pin_2);
			PORTB |= (1<<pin_1);
			PORTB |= (1<<pin_2);
			pin_register = &PINB;
		}
		case 'C': {
			DDRC &= (~(1)<<pin_1);
			DDRC &= (~(1)<<pin_2);
			PORTC |= (1<<pin_1);
			PORTC |= (1<<pin_2);
			pin_register = &PINC;
		}
		case 'D': {
			DDRC &= (~(1)<<pin_1);
			DDRC &= (~(1)<<pin_2);
			PORTC |= (1<<pin_1);
			PORTC |= (1<<pin_2);
			pin_register = &PIND;
		}
	}
}
