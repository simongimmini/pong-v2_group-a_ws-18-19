#include "gamecontrol.h"

void random_direction_generator() {
	seed = (seed*13 + 1) % 97;
	srand(seed);
	if (direction == -1 || direction == -2) {
		if (dead_player == 0) {
			if (rand() % 2 == 1) {
				direction = 1;
			}
			else {
				direction = 2;
			}
		} else if (dead_player == 2) {
			direction = 2;
		} else if (dead_player == 3) {
			direction = 1;
		}
	}
	else if (direction == 1 || direction == -3) {
		if (dead_player == 0) {
			if (rand() % 2 == 1) {
				direction = -1;
			}
			else {
				direction = 3;
			}
		} else if (dead_player == 1) {
			direction = 3;
		} else if (dead_player == 3) {
			direction = -1;
		}
	}
	else if (direction == 2 || direction == 3) {
		if (dead_player == 0) {
			if (rand() % 2 == 1) {
				direction = -2;
			}
			else {
				direction = -3;
			}
		} else if (dead_player == 1) {
			direction = -3;
		} else if (dead_player == 2) {
			direction = -2;
		}
	}
}

void use_powerup(int _which) {
	switch (_which){
		case 0: {
			// increases speed for short duration
			speed = 96;
		} break;
		case 1: {
			// delays the ball for some time
			_delay_ms(1000);
		} break;
		case 2: {
			// disappears the ball for some time
			set_brightness(0);
		} break;
		case 3: {
			// changes direction any time
			direction = direction * (-1);
		} break;
	}
}

//determine the start player and wait, until he press' his Button
void game_start(int _player) {
	show_score(_player);
	speed = 80;
	reset_stripe_array();
	if (start_player == 0) {
		coord.led = 28;
		coord.stripe = LED0_PIN;
		direction = 1;
		if (player_number == 3) {
			direction = -1;
			random_direction_generator();
		}
		set_rgb_pixel(set_ball_color(speed), 28, LED0_PIN);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);		// to prevent too long shown fail coord
		_delay_ms(500);
		button_check(b00, &button0_state);
		while(button0_state == 0) {
			button_check(b00, &button0_state);
		}
	}
	else if (start_player == 1) {
		coord.led = 28;
		coord.stripe = LED1_PIN;
		direction = -1;
		if (player_number == 3) {
			direction = 1;
			random_direction_generator();
		}
		set_rgb_pixel(set_ball_color(speed), 28, LED1_PIN);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		_delay_ms(500);
		button_check(b10, &button1_state);
		while(button1_state == 0) {
			button_check(b10, &button1_state);
		}
	} else if (start_player == 2) {
		coord.led = 28;
		coord.stripe = LED2_PIN;
		direction = -3;
		if (player_number == 3) {
			direction = 3;
			random_direction_generator();
		}
		set_rgb_pixel(set_ball_color(speed), 28, LED2_PIN);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		_delay_ms(500);
		button_check(b20, &button2_state);
		while(button2_state == 0) {
			button_check(b20, &button2_state);
		}
	}
}

void show_score(int _player) {
	struct RGB _color_of_points = nice_green;
	if(_player == 3) {
		for (int i = 1; i <= player1_points; i++) {
			set_rgb_pixel(_color_of_points, (2*i)-1, LED0_PIN);
		}
		for (int i = 1; i <= player2_points; i++) {
			set_rgb_pixel(_color_of_points, (2*i)-1, LED1_PIN);
		}
		for (int i = 1; i <= player3_points; i++) {
			set_rgb_pixel(_color_of_points, (2*i)-1, LED2_PIN);
		}
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		/* in our third 3 player game we dont want the points to be shown too long as it is a running game.
		So if the game_choice is 3 the delay only will be 500ms */
		if (game_choice < 3) {
			_delay_ms(1000);
		}
		_delay_ms(500);
		reset_stripe_array();
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);

		// checks powerups
		check_powerup(3);

	} else if(_player == 2) {
		for(int i = 1; i <= current_2player_score; i++) {
			set_rgb_pixel(pink, i, LED2_PIN);
		}
		for(int i = NMAX; i > current_2player_score; i--) {
			set_rgb_pixel(cyan, i, LED2_PIN);
		}
		send_stripe_array(LED2_PIN);
	}
}

void check_powerup(int _player) {
	if (_player == 2 && game_choice != 1) {
		if (current_2player_score < 5) {
			powerup1 = 1;
			turn_on_light_powerup(1);
		} else if (current_2player_score > 27) {
			powerup0 = 1;
			turn_on_light_powerup(0);
		}
	} else if (_player == 3 && game_choice != 1) {
		if (player1_shots == 4 || (player1_points == 1 && bonus_powerup0 == 1)) {
			powerup0 = 1;
			turn_on_light_powerup(0);
		}
		if (player2_shots == 4 || (player2_points == 1 && bonus_powerup1 == 1)) {
			powerup1 = 1;
			turn_on_light_powerup(1);
		}
		if (player3_shots == 4 || (player3_points == 1 && bonus_powerup2 == 1)) {
			powerup2 = 1;
			turn_on_light_powerup(2);
		}
		// deactivates bonus powerup if a player got only one point left
		if (player1_points == 1) {
			bonus_powerup0 = 0;
		}
		if (player2_points == 1) {
			bonus_powerup1 = 0;
		}
		if (player3_points == 1) {
			bonus_powerup2 = 0;
		}
	}
}

// just some start animation at the start of the controller
void start_animation() {
	reset_stripe_array();
	for (int i = NMAX; i >= 1; i--) {
		set_pixel(i*8-1, (255-i*8+1)%255, 255-(i*8-1), i, LED0_PIN);
		set_pixel(255-i*8+1, i*8-1, (255-i*8+1)%255, i, LED1_PIN);
		set_pixel((255-i*8+1)%255, 255-(i*8-1), i*8-1, i, LED2_PIN);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		_delay_ms(30);
	}
	for (int i = NMAX; i >= 1; i--) {
		set_rgb_pixel(blank, i, LED0_PIN);
		set_rgb_pixel(blank, i, LED1_PIN);
		set_rgb_pixel(blank, i, LED2_PIN);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		_delay_ms(30);
	}
}

// just some win animation at the end of the game
void win_animation(int _pin) {
	reset_stripe_array();

	for (int i = 1; i <= NMAX; i++) {
		set_rgb_pixel(red, i, LED0_PIN);
		set_rgb_pixel(red, i, LED1_PIN);
		set_rgb_pixel(red, i, LED2_PIN);
	}
	send_stripe_array(LED0_PIN);
	send_stripe_array(LED1_PIN);
	send_stripe_array(LED2_PIN);
	_delay_ms(500);
	for (int i = 1; i <= NMAX; i++) {
		set_rgb_pixel(nice_green, i, _pin);
		send_stripe_array(_pin);
		_delay_ms(30);
	}
	for (int i = 16; i >= 1; i--) {
		set_rgb_pixel(blank, i, _pin);
		set_rgb_pixel(blank, NMAX+1-i, _pin);
		send_stripe_array(_pin);
		_delay_ms(30);
	}
	for (int i = 1; i <= NMAX/2; i++) {
		set_rgb_pixel(nice_green, i, _pin);
		set_rgb_pixel(nice_green, NMAX+1-i, _pin);
		send_stripe_array(_pin);
		_delay_ms(50);
	}
	reset_stripe_array();
	send_stripe_array(LED0_PIN);
	send_stripe_array(LED1_PIN);
	send_stripe_array(LED2_PIN);
}

void demo_animation(int _t) {
	reset_stripe_array();
	for (int j = 1; j <= _t; j++) {
		/*for (int i = NMAX; i >= 1; i--) {
			set_pixel(j*i*8-1, (255-i*j*8+1)%255, 255-(i*j*8-1), i, LED0_PIN);
			set_pixel(255-j*i*8+1, j*i*8-1, (255-j*i*8+1)%255, i, LED1_PIN);
			set_pixel((255-j*i*8+1)%255, 255-(j*i*8-1), j*i*8-1, i, LED2_PIN);
			send_stripe_array(LED0_PIN);
			send_stripe_array(LED1_PIN);
			send_stripe_array(LED2_PIN);
			_delay_ms(30);
		}*/
		for (int i = NMAX; i >= 1; i--) {
			set_pixel(i*8-1, (255-i*8+1)%255, 255-(i*8-1), i, ((1+j)%3)+1);
			set_pixel(255-i*8+1, i*8-1, (255-i*8+1)%255, i, ((2+j)%3)+1);
			set_pixel((255-i*8+1)%255, 255-(i*8-1), i*8-1, i, ((3+j)%3)+1);
			send_stripe_array(LED0_PIN);
			send_stripe_array(LED1_PIN);
			send_stripe_array(LED2_PIN);
			_delay_ms(20);
		}
	}
	reset_stripe_array();
}

// Code for start player determination, working for 2 and 3 player
void determine_start_player(int _player) {
	standard_brightness = brightness;
	reset_stripe_array();
	send_stripe_array(LED0_PIN);
	send_stripe_array(LED1_PIN);
	send_stripe_array(LED2_PIN);
	if (_player == 2){
		for	(int i = 1; i <= 400; i++) {
			_delay_ms(10);
			button_check(b00, &button0_state);
			button_check(b10, &button1_state);
			if(button0_state == 1) {
				start_player = 1;
				break;
			}
			else if(button1_state == 1) {
				start_player = 0;
				break;
			}
		}
	} else if (_player == 3) {
		for	(int i = 1; i <= 400; i++) {
			_delay_ms(10);
			button_check(b00, &button0_state);
			button_check(b10, &button1_state);
			button_check(b20, &button2_state);
			if(button0_state == 1) {
				start_player = 1;
				break;
			} else if(button1_state == 1) {
				start_player = 2;
				break;
			} else if (button2_state == 1) {
				start_player = 0;
				break;
			}
		}
	}
	// lights up first and second led
	if (button0_state == 0 && button1_state == 0 && button2_state == 0) {
		reset_stripe_array();
		set_rgb_pixel(red, 1, LED0_PIN);
		set_rgb_pixel(red, 1, LED1_PIN);
		set_rgb_pixel(red, 1, LED2_PIN);
		set_rgb_pixel(red, 2, LED0_PIN);
		set_rgb_pixel(red, 2, LED1_PIN);
		set_rgb_pixel(red, 2, LED2_PIN);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		if (_player == 2) {
			do {
				button_check(b00, &button0_state);
				button_check(b10, &button1_state);
			} while ((button0_state == 0) && (button1_state == 0));
		} else {
			do {
				button_check(b00, &button0_state);
				button_check(b10, &button1_state);
				button_check(b20, &button2_state);
			} while ((button0_state == 0) && (button1_state == 0) && (button2_state == 0));
		}
		reset_stripe_array();
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		if (button0_state == 1) {
			start_player = 0;
		} else if (button1_state == 1) {
			start_player = 1;
		} else if (_player == 3 && button2_state == 1) {
			start_player = 2;
		}
	}

	// some initialization
	current_2player_score = NMAX/2;
	player1_points = 5;
	player2_points = 5;
	player3_points = 5;
	powerup0 = 0;
	powerup1 = 0;
	powerup2 = 0;
	bonus_powerup0 = 1;
	bonus_powerup1 = 1;
	bonus_powerup2 = 1;
	dead_player = 0;
	seed = 1933;
}

// moving the ball when no buttons are pressed, also works with 2 player mode (direction is only -1 or 1)
void move_ball_if_nothing_pressed() {
	if ((direction == 1 || direction == -3) && coord.led < NMAX+1 && coord.stripe == LED1_PIN) {
		coord.led = coord.led + 1;
	}
	if ((direction == 1 || direction == 2) && coord.led >= 1 && coord.stripe == LED0_PIN) {
		if(coord.led == 1 && direction == 2) {
			coord.stripe = LED2_PIN;
		}
		else if (coord.led == 1 && direction == 1) {
			coord.stripe = LED1_PIN;
		}
		else {
			coord.led = coord.led - 1;
		}
	}

	if ((direction == -1 || direction == -2) && coord.led < NMAX+1 && coord.stripe == LED0_PIN) {
		coord.led = coord.led + 1;
	}
	if ((direction == -1 || direction == 3) && coord.led >= 1 && coord.stripe == LED1_PIN) {
		if(coord.led == 1 && direction == -1) {
			coord.stripe = LED0_PIN;
		}
		else if (coord.led == 1 && direction == 3) {
			coord.stripe = LED2_PIN;
		}
		else {
			coord.led = coord.led - 1;
		}
	}

	if ((direction == 2 || direction == 3) && coord.led < NMAX+1 && coord.stripe == LED2_PIN) {
		coord.led = coord.led + 1;
	}
	if ((direction == -2 || direction == -3) && coord.led >= 1 && coord.stripe == LED2_PIN) {
		if(coord.led == 1 && direction == -2) {
			coord.stripe = LED0_PIN;
		}
		else if (coord.led == 1 && direction == -3) {
			coord.stripe = LED1_PIN;
		}
		else {
			coord.led = coord.led - 1;
		}
	}
}

//changes direction of ball when buttons are pressed correctly and adjusts speed
void move_ball_if_correctly_pressed() {
	if ((direction == -1 || direction == -2) && coord.led > 25 && coord.stripe == LED0_PIN && button0_state == 1 && button0_state_last_round != 1) {
		if (player_number == 3) {
			random_direction_generator();
			player1_shots++;
			check_powerup(3);
		} else {
			direction = 1;
		}
		speed = 100 - ((32 - coord.led) * 4);
	}
	if ((direction == 1 || direction == -3) && coord.led > 25 && coord.stripe == LED1_PIN && button1_state == 1  && button1_state_last_round != 1) {
		if (player_number == 3) {
			random_direction_generator();
			player2_shots++;
			check_powerup(3);
		} else {
			direction = -1;
		}
		speed = 100 - ((32 - coord.led) * 4);
	}
	if ((direction == 2 || direction == 3) && coord.led > 25 && coord.stripe == LED2_PIN && button2_state == 1  && button2_state_last_round != 1) {
		random_direction_generator();
		player3_shots++;
		check_powerup(3);
		speed = 100 - ((32 - coord.led) * 4);
	}
}

// activates powerups
void activate_powerups() {
	if (game_choice > 1) {
		if (powerup0 == 1 && powerup_button0_state == 1 && ((coord.led <= 15) || ((chosen_powerup0 == 1) && coord.led < 24))) {
			use_powerup(chosen_powerup0);
			powerup0 = 0;
			turn_off_light_powerup(0);
			player1_shots = 0;
		} else if (powerup1 == 1 && powerup_button1_state == 1 && ((coord.led <= 15) || ((chosen_powerup1 == 1) && coord.led < 24))) {
			use_powerup(chosen_powerup1);
			powerup1 = 0;
			turn_off_light_powerup(1);
			player2_shots = 0;
		} else if (player_number == 3 && powerup2 == 1 && powerup_button2_state == 1 && ((coord.led <= 15) || ((chosen_powerup2 == 1) && coord.led < 24))) {
			use_powerup(chosen_powerup2);
			powerup2 = 0;
			turn_off_light_powerup(2);
			player3_shots = 0;
		}
		// sets the standard_brightness at specific point after using the disappearing powerup
		int _activation_point = 16;
		if (((direction == 2 || direction == 3) && coord.led == _activation_point && coord.stripe == LED2_PIN) ||
	    ((direction == 1 || direction == -3) && coord.led == _activation_point && coord.stripe == LED1_PIN) ||
	    ((direction == -2 || direction == -1) && coord.led == _activation_point && coord.stripe == LED0_PIN)) {
			set_brightness(standard_brightness);
		}

	}
}

// puts the ball to end (NMAX) if button is pressed wrong
void move_ball_if_wrong_pressed() {
	if (((direction == -1 || direction == -2) && coord.led <= 25 && button0_state == 1 && button0_state_last_round != 1 && dead_player != 1) ||
	((coord.stripe == LED1_PIN || coord.stripe == LED2_PIN) && button0_state == 1  && button0_state_last_round != 1 && dead_player != 1)) {
		coord.led = NMAX;
		coord.stripe = LED0_PIN;
	}
	if (((direction == 1 || direction == -3) && coord.led <= 25 && button1_state == 1 && button1_state_last_round != 1 && dead_player != 2) ||
	((coord.stripe == LED0_PIN || coord.stripe == LED2_PIN) && button1_state == 1  && button1_state_last_round != 1 && dead_player != 2)) {
		coord.led = NMAX;
		coord.stripe = LED1_PIN;
	}
	if (((direction == 2 || direction == 3) && coord.led <= 25 && button2_state == 1 && button2_state_last_round != 1 && dead_player != 3) ||
	((coord.stripe == LED0_PIN || coord.stripe == LED1_PIN) && button2_state == 1  && button2_state_last_round != 1 && dead_player != 3)) {
		coord.led = NMAX;
		coord.stripe = LED2_PIN;
	}
}
