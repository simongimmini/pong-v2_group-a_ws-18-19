#ifndef MENU_H
#define MENU_H

// defines pin of stripe used for menu setting
#define PLAYER_MENU_PIN LED0_PIN

// define the positions of the menupoints on the stripe
#define MENU_START_POINT	2
#define MENU_INCREMENT		4

#include <avr/io.h>
#include <util/delay.h>
#include "buttoncontrol.h"
#include "ledcommunication.h"
#include "gamecontrol.h"

//normal Buttons
int normal_button0_state;
int normal_button1_state;
int normal_button2_state;
//LED Buttons
int LED_button0_state;
int LED_button1_state;
int LED_button2_state;

int game_choice;
int player_number;
int quit;
int menu_choice;
int button_holdtime_numerator;
int menu_max;

// starts player menu
void player_menu();
// sets menu brightness
void brightness_menu();
// starts the (selected) gamemode
void game_menu();
// powerup menu
void powerup_menu();
// function to reduce code in every menu function. Checks all buttons
void wait_until_button_pressed();
// lights up selected menu point
void show_menu_light(int _i, int _menu);
/*  flashes selected menu point
    menu = 0 - player with brightness
    menu = 1 - modes
    menu = 2 - powerups */
void show_selection(int _selection, int _menu);
// handles selection
int menu_point_selection(int _number, int *_selection, int _menu);
int brightness_selection(int _number);
int powerup_selection(int _number, int _menu);

#endif // MENU_H
