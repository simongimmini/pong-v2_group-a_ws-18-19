#ifndef GAMECONTROL_H
#define GAMECONTROL_H

#define INITIAL_SPEED 80
#define NMAX 32

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "buttoncontrol.h"
#include "ledcommunication.h"
#include "menu.h"

int direction;
int speed;
int current_2player_score;
int start_player;

// stores dead player in 3 player mode
int dead_player;
// seed for random generator
int seed;

int button0_state;
int button1_state;
int button2_state;

int powerup_button0_state;
int powerup_button1_state;
int powerup_button2_state;

// stores last button state (pressed or not pressed) to avoid multiple button signals in one press
int button0_state_last_round;
int button1_state_last_round;
int button2_state_last_round;

// declares int for powerup (0 = false, 1 = true)
int powerup0;
int powerup1;
int powerup2;
// chosen powerup of each player
int chosen_powerup0;
int chosen_powerup1;
int chosen_powerup2;
// bonus powerup for threeplayer mode (if a player only got one point left, he gets a powerup)
int bonus_powerup0;
int bonus_powerup1;
int bonus_powerup2;

int player1_points;
int player2_points;
int player3_points;

int player1_shots;
int player2_shots;
int player3_shots;

struct coordinates {
	int led;
	int stripe;
};

struct coordinates coord;
struct coordinates fail_coord;

//int determine_start_player();
//int direction_selection(int _hit, int _hit_LED);
//void determine_start_player_2player();
void game_start(int _player);
void show_score(int _player);
void check_powerup(int _player);
void start_animation();
void win_animation(int _pin);
void demo_animation(int _t);
void determine_start_player(int _player);
void random_direction_generator();
void use_powerup(int _which);
void move_ball_if_nothing_pressed();
void move_ball_if_correctly_pressed();
void activate_powerups();
void move_ball_if_wrong_pressed();

#endif // GAMECONTROL_H
