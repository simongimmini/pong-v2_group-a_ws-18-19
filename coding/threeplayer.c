#include "threeplayer.h"

void update_score_3player() {
	if(coord.led == NMAX) {
		reset_stripe_array();
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);

		// reset shot counter after each round
		player1_shots = 0;
		player2_shots = 0;
		player3_shots = 0;

		if (coord.stripe == LED0_PIN) {
			player1_points -= 1;
			start_player = 0;
		} else if (coord.stripe == LED1_PIN) {
			player2_points -= 1;
			start_player = 1;
		} else if (coord.stripe == LED2_PIN) {
			player3_points -= 1;
			start_player = 2;
		}
		//Fail LED blink
		set_rgb_pixel(red, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(400);
		set_rgb_pixel(blank, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(200);
		set_rgb_pixel(red, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(400);
		set_rgb_pixel(blank, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(200);
		set_rgb_pixel(red, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(400);
		reset_stripe_array();

		// checks powerups
		check_powerup(3);

		// checks if a player is eliminated and adjusts directions
		if (player1_points == 0 && (player2_points && player3_points) > 0 && dead_player == 0) {
			start_player = 1;
			dead_player = 1;
		} else if (player2_points == 0 && (player1_points && player3_points) > 0 && dead_player == 0) {
			start_player = 2;
			dead_player = 2;
		} else if (player3_points == 0 && (player1_points && player2_points) > 0 && dead_player == 0) {
			start_player = 0;
			dead_player = 3;
		}

		// starts new serve
		if ((player1_points > 0 && player2_points > 0) || (player1_points > 0 && player3_points > 0) || (player2_points > 0 && player3_points > 0)) {
			game_start(3);
		}
	}
}

// Player 0 -> 1 direction 1; Player 1 -> 0 direction -1; Player 0 -> 2 direction 2; Player 2 -> 0 direction -2; Player 1 -> 2 direction 3; Player 2 -> 1 direction -3
void move_ball_3player(int _gamemode) {
	button_check(b00, &button0_state);
	button_check(b10, &button1_state);
	button_check(b20, &button2_state);
	button_check(b01, &powerup_button0_state);
	button_check(b11, &powerup_button1_state);
	button_check(b21, &powerup_button2_state);

	fail_coord = coord;


	if (coord.led == NMAX-1) {
		fail_coord.led = NMAX;
	}
	/*moving the ball when no buttons are pressed*/
	move_ball_if_nothing_pressed();

	/*changes direction of ball when buttons are pressed correctly and adjusts speed*/
	move_ball_if_correctly_pressed();

	/*puts the ball to either end if button is pressed wrong*/
	move_ball_if_wrong_pressed();

	/*activates powerups */
	activate_powerups();

	for (int j = 100; j > speed; j--){
		_delay_ms(1);
	}

	button0_state_last_round = button0_state;
	button1_state_last_round = button1_state;
	button2_state_last_round = button2_state;
}

void move_ball2_3player(int _gamemode){
	button_check(b00, &button0_state);
	button_check(b10, &button1_state);
	button_check(b20, &button2_state);
	button_check(b01, &powerup_button0_state);
	button_check(b11, &powerup_button1_state);
	button_check(b21, &powerup_button2_state);

	fail_coord = coord;


	if (coord.led == NMAX-1) {
		fail_coord.led = NMAX;
	}

	// calls function for ball movement if nothing pressed
	move_ball_if_nothing_pressed();

	// changes stripe if led coord is NMAX
	if (coord.led == NMAX + 1 && coord.stripe == LED0_PIN) {
		if (direction == -1) {
			coord.stripe = LED1_PIN;
			player1_points -= 1;
		} else if (direction == -2) {
			coord.stripe = LED2_PIN;
			player1_points -= 1;
		}
		speed -= 3;
		coord.led = NMAX;
		if (player1_points < 0) {
			player1_points = 0;
		}
		show_score(3);
	}
	if (coord.led == NMAX + 1 && coord.stripe == LED1_PIN) {
		if (direction == 1) {
			coord.stripe = LED0_PIN;
			player2_points -= 1;
		} else if (direction == -3) {
			coord.stripe = LED2_PIN;
			player2_points -= 1;
		}
		speed -= 3;
		coord.led = NMAX;
		if (player2_points < 0) {
			player2_points = 0;
		}
		show_score(3);
	}
	if (coord.led == NMAX + 1 && coord.stripe == LED2_PIN) {
		if (direction == 2) {
			coord.stripe = LED0_PIN;
			player3_points -= 1;
		} else if (direction == 3) {
			coord.stripe = LED1_PIN;
			player3_points -= 1;
		}
		speed -= 3;
		coord.led = NMAX;
		if (player3_points < 0) {
			player3_points = 0;
		}
		show_score(3);
	}
	//changes direction of ball when buttons are pressed correctly and adjusts speed
	move_ball_if_correctly_pressed();

	//updates score if button is pressed wrong
	if ((((direction == -1 || direction == -2) && coord.led <= 25 && button0_state == 1 && button0_state_last_round != 1) ||
	((coord.stripe == LED1_PIN || coord.stripe == LED2_PIN) && button0_state == 1  && button0_state_last_round != 1)) && (player1_points > 0)) {
		player1_points -= 1;
		player1_shots = 0;
		player2_shots = 0;
		player3_shots = 0;
	}
	if ((((direction == 1 || direction == -3) && coord.led <= 25 && button1_state == 1 && button1_state_last_round != 1) ||
	((coord.stripe == LED0_PIN || coord.stripe == LED2_PIN) && button1_state == 1  && button1_state_last_round != 1)) && (player2_points > 0)) {
		player2_points -= 1;
		player1_shots = 0;
		player2_shots = 0;
		player3_shots = 0;
	}
	if ((((direction == 1 || direction == 2) && coord.led <= 25 && button2_state == 1 && button2_state_last_round != 1) ||
	((coord.stripe == LED0_PIN || coord.stripe == LED1_PIN) && button2_state == 1  && button2_state_last_round != 1)) && (player3_points > 0)) {
		player3_points -= 1;
		player1_shots = 0;
		player2_shots = 0;
		player3_shots = 0;
	}

	//sets dead_player if points == 0 and changes direction
	if (dead_player == 0) {
		if (player1_points == 0) {
			dead_player = 1;
			if (direction == -1) {
				direction = 3;
			} else {
				direction = -3;
			}
		} else if (player2_points == 0) {
			dead_player = 2;
			if (direction == 1) {
				direction = 2;
			} else {
				direction = -2;
			}
		} else if (player3_points == 0) {
			dead_player = 3;
			if (direction == 2) {
				direction = 1;
			} else {
				direction = -1;
			}
		}
	}

	//activates powerups
	activate_powerups();

	for (int j = 100; j > speed; j--){
		_delay_ms(1);
	}

	button0_state_last_round = button0_state;
	button1_state_last_round = button1_state;
	button2_state_last_round = button2_state;
}
