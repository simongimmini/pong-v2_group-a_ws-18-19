#include "menu.h"

/*  function handles depending on menu_choice how many players are playing */
void player_menu() {
	menu_max = 4;
	menu_choice = 1;
	quit = 0;
	while (quit == 0) {
	    switch (menu_choice) {
	        case 1: {
	    		if (menu_point_selection(1, &player_number, 0)) break;
	        }
	        case 2: {
	            if (menu_point_selection(2, &player_number, 0)) break;
	        }
	        case 3: {
	            if (menu_point_selection(3, &player_number, 0)) break;
	        }
	        case 4: {
	        	if (menu_point_selection(4, &player_number, 0)) {
	        		brightness_menu();
	        		break;
	        	}
	        }
	        default: {
	            quit = 0;
	            menu_choice = 1;
	        }
	    }
	}
	quit = 0;
	_delay_ms(500);
	game_menu();
}

// select brightness of the game
void brightness_menu() {
    menu_max = 3;
    quit = 0;
    menu_choice = 1;

    while (quit == 0) {
        switch (menu_choice) {
            case 1: {
                if (brightness_selection(1)) break;
            }
            case 2: {
                if (brightness_selection(2)) break;
            }
            case 3: {
                if (brightness_selection(3)) break;
            }
            default: {
                quit = 0;
                menu_choice = 1;
            }
        }
    }
    quit = 0;
}

void game_menu() {
    if(player_number != 1) {
		menu_max = 3;
		quit = 0;
		menu_choice = 1;
		while (quit == 0) {
		    switch (menu_choice) {
		        case 1: {
		            if (menu_point_selection(1, &game_choice, 1)) break;
		        }
		        case 2: {
		            if (menu_point_selection(2, &game_choice, 1)) break;
		        }
		        case 3: {
		            if (menu_point_selection(3, &game_choice, 1)) break;
		        }
		        default: {
		            quit = 0;
		            menu_choice = 1;
		        }
		    }
		}
		quit = 0;
		_delay_ms(500);
		if(game_choice != 1) {
			powerup_menu();
		}
	}
}

void powerup_menu(){
    menu_max = 4;
    quit = 0;
    menu_choice = 1;
    chosen_powerup0 = -1;
    chosen_powerup1 = -1;
    chosen_powerup2 = -1;
    while (quit == 0) {
        switch (menu_choice) {
            case 1: {
                if (powerup_selection(1,2)) break;
            }
            case 2: {
                if (powerup_selection(2,2)) break;
            }
            case 3: {
                if (powerup_selection(3,2)) break;
            }
            case 4: {
                if (powerup_selection(4,2)) break;
            }
            default: {
                quit = 0;
                menu_choice = 1;
            }
        }
    }
    quit = 0;
    _delay_ms(500);
}

void wait_until_button_pressed(){
    do {
        button_check(b00, &normal_button0_state);
        button_check(b10, &normal_button1_state);
        button_check(b20, &normal_button2_state);

        button_check(b01, &LED_button0_state);
        button_check(b11, &LED_button1_state);
        button_check(b21, &LED_button2_state);
    } while ((normal_button0_state == 0) && (normal_button1_state == 0) && (normal_button2_state == 0) &&
            (LED_button0_state == 0) && (LED_button1_state == 0) && (LED_button2_state == 0));
}

void show_menu_light(int _i, int _menu) {
    reset_stripe_array();
    set_rgb_pixel(nice_green, MENU_START_POINT + (_i * MENU_INCREMENT), _menu + 1);
    send_stripe_array(_menu + 1);
}

void show_selection(int _i, int _menu) {
	for (int j = 0; j < 3; j++) {
	    reset_stripe_array();
	    set_rgb_pixel(blue, MENU_START_POINT + (_i * MENU_INCREMENT), _menu + 1);
	    _delay_ms(250);
	    reset_stripe_array();
	    send_stripe_array(_menu + 1);
	    _delay_ms(250);
	}
}

/*  _number is the iterator where the led is shining counting from 1 to n
    _selection is the point selected after pressing the pressing the button
    _menu = player(0), mode(1), powerup(2)
    So number is the current illuminated dot, selection is the parameter saved by pressing and menu determines the different
    menu types (we don't want a function for each menu_type)
*/
int menu_point_selection(int _number, int *_selection, int _menu) {
    show_menu_light(_number, _menu);
    // waits for a buttonpress
    wait_until_button_pressed();

    // by pressing the powerup button you confirm the selection
    if ((LED_button0_state == 1) || (LED_button1_state == 1) || (LED_button2_state == 1)) {
        *_selection = _number;
        // ends switch-loop in menu()
        quit = 1;
        show_selection(_number, _menu);
        // sets the selected case
        return(1);
    }

    // jumps to next menu point by pressing push buttons
    if (menu_choice < menu_max) menu_choice++;
    // jumps back to first menu point after reaching the end of menu
    else if (menu_choice == menu_max) menu_choice = 1;
    reset_stripe_array();
    send_stripe_array(_menu + 1);
    _delay_ms(500);
    return(0);
}

int brightness_selection(int _number) {
    // sets the brightness
    switch (_number) {
        case 1: set_brightness(0.8);
                break;
        case 2: set_brightness(0.6);
                break;
        case 3: set_brightness(0.3);
                break;
    }
    for (int i = 1; i <= NMAX; i++) {
        set_rgb_pixel(white, i, LED0_PIN);
    }
    send_stripe_array(LED0_PIN);

    // confirm the selection by pressing a powerup button
    wait_until_button_pressed();

    // after confirmation the stripe slowly fades out
    if ((LED_button0_state == 1) || (LED_button1_state == 1) || (LED_button2_state == 1)) {
        quit = 1;
        // slowly fades out LEDs
        for (int i = 0; i <= 255; i++) {
            for (int j = 0; j <= NMAX; j++) {
                set_pixel(255-i, 255-i, 255-i, j, LED0_PIN);
            }
            send_stripe_array(LED0_PIN);
        }
        return(1);
    }
    _delay_ms(500);

    // jumps to next brightness value
    if (menu_choice < menu_max) {
        menu_choice++;
    }
    // jumps back to the first brightness value
    else if (menu_choice == menu_max) {
        menu_choice = 1;
    }
    return(0);
}

int powerup_selection(int _number, int _menu) {
	while (chosen_powerup0 == -1 || chosen_powerup1 == -1 || (chosen_powerup2 == -1 && player_number == 3)) {
		show_menu_light(_number, _menu);
		wait_until_button_pressed();
		if (LED_button0_state == 1 && chosen_powerup0 == -1) {
		    chosen_powerup0 = _number - 1;
		    show_selection(_number, _menu);
		} else if (LED_button1_state == 1 && chosen_powerup1 == -1) {
		    chosen_powerup1 = _number - 1;
		    show_selection(_number, _menu);
		} else if (LED_button2_state == 1 && player_number == 3 && chosen_powerup2 == -1) {
		    chosen_powerup2 = _number - 1;
		    show_selection(_number, _menu);
		}
	    if (menu_choice < menu_max) menu_choice++;
    	else if (menu_choice == menu_max) menu_choice = 1;
    	reset_stripe_array();
    	send_stripe_array(_menu + 1);
    	_delay_ms(300);
    	return(0);
	}
	quit = 1;
	return(1);
}
