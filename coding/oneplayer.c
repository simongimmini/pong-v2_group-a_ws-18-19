#include "oneplayer.h"

void move_ball_1player() {	
	button_check(b00, &button0_state);
	button_check(b01, &button1_state);

	fail_coord = coord;

	if (coord.led == NMAX-1) {					
		fail_coord.led = NMAX;
	}

	
	/*moving the ball when no buttons are pressed*/
	if (direction == 1 && coord.led < NMAX + 1 && coord.stripe == LED0_PIN) {
		coord.led = coord.led + 1;
	}
	if (direction == -1 && coord.led >= 1 && coord.stripe == LED0_PIN) {
		if(coord.led == 1) {
			coord.stripe = LED0_PIN;
			direction = 1;
			speed += 1;
		}
		else {
			coord.led = coord.led - 1;
		}
	}

	/*changes direction of ball when buttons are pressed correctly and adjusts speed*/
	if (direction == 1 && coord.led > 25 && coord.stripe == LED0_PIN && button0_state == 1  && button0_state_last_round != 1) {
		direction = -1;
	}
	
	//changes direction in the end
	if (direction == -1 && coord.led == 0) {
		direction = 1;
		speed += 3;
	}
	
	//makes mistake in the end
	if(coord.led == NMAX + 1) {
		coord.led = 29;
		speed = 75;
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		direction = -1;
		do {
			button_check(b00, &button0_state);
		} while (button0_state == 0 && button0_state_last_round != 1);
	}
	
	/*puts the ball to start point if button is pressed wrong*/
	if ((direction == -1 && coord.led <= 25 && button0_state == 1 && button0_state_last_round != 1) || (direction == 1 && coord.led <= 25 && button0_state == 1  && button0_state_last_round != 1)) {
		coord.led = 29;
		speed = 75;
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		direction = -1;
		_delay_ms(200);
		do {
			button_check(b00, &button0_state);	
			button_check(b01, &button1_state);
			if (button1_state == 1) {
				break;
			}	
		} while (button0_state == 0 && button0_state_last_round != 1);
	}
	
		
	for (int j = 100; j > speed; j--){
		_delay_ms(1);
	}
	
	if(speed == 98) {
		for (int i = NMAX; i >= 1; i--) {
			set_rgb_pixel(green, i, LED0_PIN);
		}
		send_stripe_array(LED0_PIN);
		_delay_ms(2000);
		run_menu();
		//speed = 75;
	}

	button0_state_last_round = button0_state;
}

