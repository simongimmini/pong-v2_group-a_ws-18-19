#include <avr/io.h>

#ifndef BUTTONCONTROL_H
#define BUTTONCONTROL_H

#define bit_read(x,n) ((x) >> (n)) &1
#define bit_clear(x,n) ((x) &= ~(1<<n))
#define bit_set(x,n) ((x) |= (1<<(n)))

// DDR and PORT for buttons
#define BUTTON_DDR DDRC
#define BUTTON_PORT PORTC

// DDR and PORT for LED of illuminated buttons
#define BUTTON_LED_DDR DDRD
#define BUTTON_LED_PORT PORTD

// define pins for push buttons
// "ButtonX_PlayerY_PIN"
#define BTN0_PL0_PIN 0
#define BTN0_PL1_PIN 2
#define BTN0_PL2_PIN 1

// define pins for powerup buttons
#define BTN1_PL0_PIN 3
#define BTN1_PL1_PIN 5
#define BTN1_PL2_PIN 4

// define pins for leds of powerup buttons
#define BTN1_PL0_LED_PIN 2
#define BTN1_PL1_LED_PIN 0
#define BTN1_PL2_LED_PIN 1

// each button has a pin and type (0 = push, 1 = powerup)
struct button {
    uint8_t pin;
    uint8_t type;
};

// player 1
static struct button b00 = {BTN0_PL0_PIN, 0};
static struct button b01 = {BTN1_PL0_PIN, 1};

// player 2
static struct button b10 = {BTN0_PL1_PIN, 0};
static struct button b11 = {BTN1_PL1_PIN, 1};

// player 3
static struct button b20 = {BTN0_PL2_PIN, 0};
static struct button b21 = {BTN1_PL2_PIN, 1};

static struct button buttons[6];

void button_check(struct button _button, int *_state);
void button_init();
void led_button_init();
void turn_on_light_powerup(int _powerup);
void turn_off_light_powerup(int _powerup);


#endif // BUTTONCONTROL_H
