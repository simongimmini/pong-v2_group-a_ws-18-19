#include "twoplayer.h"

void update_score_2player() {
	if(coord.led == NMAX) {
		reset_stripe_array();
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		if(coord.stripe == LED0_PIN) {
			current_2player_score += 4;
			start_player = 0;
		}
		else if (coord.stripe == LED1_PIN) {
			current_2player_score -= 4;
			start_player = 1;
		}
		show_score(2);
		//Fail LED blink
		set_rgb_pixel(red, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(400);
		set_rgb_pixel(blank, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(200);
		set_rgb_pixel(red, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(400);
		set_rgb_pixel(blank, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(200);
		set_rgb_pixel(red, fail_coord.led, fail_coord.stripe);
		send_stripe_array(fail_coord.stripe);
		_delay_ms(400);
		reset_stripe_array();

		// starts new serve
		if (current_2player_score != 0 && current_2player_score != NMAX) {
			check_powerup(2);
			game_start(2);
		}
	}
}

// Player 0 -> 1 direction 1; Player 1 -> 0 direction -1
void move_ball_2player(int _gamemode) {				
	button_check(b00, &button0_state);
	button_check(b10, &button1_state);
	button_check(b01, &powerup_button0_state);
	button_check(b11, &powerup_button1_state);

	fail_coord = coord;
	if (coord.led == NMAX-1) {
		fail_coord.led = NMAX;
	}
	/*moving the ball when no buttons are pressed*/
	move_ball_if_nothing_pressed();

	/*changes direction of ball when buttons are pressed correctly and adjusts speed*/
	move_ball_if_correctly_pressed();

	/*puts the ball to either end if button is pressed wrong*/
	move_ball_if_wrong_pressed();
	
	/*activates powerups */
	activate_powerups();
	
	for (int j = 100; j > speed; j--){
		_delay_ms(1);
	}

	button0_state_last_round = button0_state;
	button1_state_last_round = button1_state;
}

// third 2player mode 
void move_ball2_2player() {
	button_check(b00, &button0_state);
	button_check(b10, &button1_state);
	button_check(b01, &powerup_button0_state);
	button_check(b11, &powerup_button1_state);

	/*moving the ball when no buttons are pressed*/
	move_ball_if_nothing_pressed();

	/*changes direction of ball when buttons are pressed correctly and adjusts speed*/
	move_ball_if_correctly_pressed();

	/*updates score if button is pressed wrong*/
	if (((direction == -1 && coord.led <= 25 && button0_state == 1) || (coord.stripe == LED1_PIN && button0_state == 1)) && (button0_state_last_round != 1)) {
		current_2player_score += 4;
		show_score(2);
	} 
	if (((direction == 1 && coord.led <= 25 && button1_state == 1) || (coord.stripe == LED0_PIN && button1_state == 1)) && (button1_state_last_round != 1)) {
		current_2player_score -= 4;
		show_score(2);
	}

	/* changes stripe if led coord is NMAX */
	if (coord.led == NMAX + 1) {
		if (coord.stripe == LED0_PIN) {
			coord.stripe = LED1_PIN;
			current_2player_score += 4;
		} else {
			coord.stripe = LED0_PIN;
			current_2player_score -= 4;
		}
		speed -= 3;
		coord.led = NMAX;
		show_score(2);
	}
	
	/*activates powerups */
	activate_powerups();
	
	/* delays depending on speed */
	for (int j = 100; j > speed; j--){
		_delay_ms(1);
	}
	button0_state_last_round = button0_state;
	button1_state_last_round = button1_state;
}

