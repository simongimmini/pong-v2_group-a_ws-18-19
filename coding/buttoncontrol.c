#include "buttoncontrol.h"

static volatile uint8_t *pin_register;

/* needs a button and the state of this button and sets the state to either
  1 if pressed or
  0 if not pressed */
void button_check(struct button _button, int *_state){
    if (bit_read(PINC, _button.pin) == 1) {
        *_state = 0;
    } else {
        *_state = 1;
    }
}

/* initializes a button at PORTC and DDRC */
void button_init() {
	DDRC=0x00;
	buttons[0] = b00;
    buttons[1] = b01;
    buttons[2] = b10;
    buttons[3] = b11;
    buttons[4] = b20;
    buttons[5] = b21;
    for (int i = 0; i < (sizeof(buttons)/sizeof(buttons[0])); i++) {
        // set button-pins on DDRC to input
        bit_clear(BUTTON_DDR, buttons[i].pin);
        // set button-pins on PORTC to HIGH
        bit_set(BUTTON_PORT, buttons[i].pin);
    }
    pin_register = &BUTTON_PORT;
}

// initialize LED pins
void led_button_init() {
	DDRD=0x00;
	DDRD |= (1 << BTN1_PL0_LED_PIN);
    DDRD |= (1 << BTN1_PL1_LED_PIN);
    DDRD |= (1 << BTN1_PL2_LED_PIN);
    // set led-button-pins on PORTD to LOW
    bit_clear(BUTTON_LED_PORT, BTN1_PL0_LED_PIN);
    bit_clear(BUTTON_LED_PORT, BTN1_PL1_LED_PIN);
    bit_clear(BUTTON_LED_PORT, BTN1_PL2_LED_PIN);
}

// function to light up the powerup LED on button
void turn_on_light_powerup(int _powerup) {
    if (_powerup == 0) {
        bit_set(BUTTON_LED_PORT, BTN1_PL0_LED_PIN);
    }
    else if (_powerup == 1) {
        bit_set(BUTTON_LED_PORT, BTN1_PL1_LED_PIN);
    }
    else if (_powerup == 2) {
    	bit_set(BUTTON_LED_PORT, BTN1_PL2_LED_PIN);
    }
}

// function to light up the powerup LED on button
void turn_off_light_powerup(int _powerup) {
    if (_powerup == 0) {
        bit_clear(BUTTON_LED_PORT, BTN1_PL0_LED_PIN);
    }
    else if (_powerup == 1) {
        bit_clear(BUTTON_LED_PORT, BTN1_PL1_LED_PIN);
    }
    else if (_powerup == 2) {
    	bit_clear(BUTTON_LED_PORT, BTN1_PL2_LED_PIN);
    }
}
