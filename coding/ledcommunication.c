#include "ledcommunication.h"

void send_bit1(int _delay) {
	if (_delay == 1) {
		//needs to be longer than 500ns including the ticks needed to clear and set the bit!
		bit_set(LED_PORT, LED0_PIN);
		delay_ns(T1H);
		bit_clear(LED_PORT, LED0_PIN);
	} else if (_delay == 0) {
		//needs to be shorter than 500ns including the ticks needed to clear and set the bit!
		bit_set(LED_PORT, LED0_PIN);
		delay_ns(T0H);
		bit_clear(LED_PORT, LED0_PIN);
	}
}

void send_bit2(int _delay) {
	if (_delay == 1) {
		//needs to be longer than 500ns including the ticks needed to clear and set the bit!
		bit_set(LED_PORT, LED1_PIN);
		delay_ns(T1H);
		bit_clear(LED_PORT, LED1_PIN);
	} else if (_delay == 0) {
		//needs to be shorter than 500ns including the ticks needed to clear and set the bit!
		bit_set(LED_PORT, LED1_PIN);
		delay_ns(T0H);
		bit_clear(LED_PORT, LED1_PIN);
	}
}

void send_bit3(int _delay) {
	if (_delay == 1) {
		//needs to be longer than 500ns including the ticks needed to clear and set the bit!
		bit_set(LED_PORT, LED2_PIN);
		delay_ns(T1H);
		bit_clear(LED_PORT, LED2_PIN);
	} else if (_delay == 0) {
		//needs to be shorter than 500ns including the ticks needed to clear and set the bit!
		bit_set(LED_PORT, LED2_PIN);
		delay_ns(T0H);
		bit_clear(LED_PORT, LED2_PIN);
	}
}


// sets color and brightness for LEDs (stored in stripe_array)
void set_pixel(unsigned char _R, unsigned char _G, unsigned char _B,
			   unsigned char _N, int _stripe_pin) {
    switch (_stripe_pin) {
        case 1: {
            stripe0_array[(_N-1)*3] = _G * brightness;
            stripe0_array[(_N-1)*3+1] = _R * brightness;
            stripe0_array[(_N-1)*3+2] = _B * brightness;
            break;
        };
        case 2: {
            stripe1_array[(_N-1)*3] = _G * brightness;
            stripe1_array[(_N-1)*3+1] = _R * brightness;
            stripe1_array[(_N-1)*3+2] = _B * brightness;
            break;
        };
        case 3: {
            stripe2_array[(_N-1)*3] = _G * brightness;
            stripe2_array[(_N-1)*3+1] = _R * brightness;
            stripe2_array[(_N-1)*3+2] = _B * brightness;
            break;
        };
    }
}

void set_rgb_pixel(struct RGB _rgb, unsigned char _N, int _stripe_pin){
	switch (_stripe_pin) {
		case 1: {
			stripe0_array[(_N-1)*3] = _rgb.g * brightness;
			stripe0_array[(_N-1)*3+1] = _rgb.r * brightness;
			stripe0_array[(_N-1)*3+2] = _rgb.b * brightness;
			break;
		};
		case 2: {
			stripe1_array[(_N-1)*3] = _rgb.g * brightness;
			stripe1_array[(_N-1)*3+1] = _rgb.r * brightness;
			stripe1_array[(_N-1)*3+2] = _rgb.b * brightness;
			break;
		};
		case 3: {
			stripe2_array[(_N-1)*3] = _rgb.g * brightness;
			stripe2_array[(_N-1)*3+1] = _rgb.r * brightness;
			stripe2_array[(_N-1)*3+2] = _rgb.b * brightness;
			break;
		};
	}
}

// resets the whole field
void reset_stripe_array() {
    for (int i = 0; i < (NMAX * 3) + 1; i++){
        stripe0_array[i] = 0;
        stripe1_array[i] = 0;
        stripe2_array[i] = 0;
    }
}

// returns number of pixels / LEDs
int number_of_pixels() {
    return(NMAX);
}

// sends RGB-information to the LEDs
void send_stripe_array(int _pin) {
    switch (_pin) {
        case 1: {
        	//goes through every entry of the array
            for (int i = 0; i < NMAX * 3; i++) {
	            //sends the bits in reversed order
                for (int j = 0; j < 8; j++) {
                	//gives the values from pixelarray to send_bit(int a) in reversed order!
                    send_bit1(bit_read(stripe0_array[i],(7-j)));
                }
            }
            break;
        };
        case 2: {
            for (int i = 0; i < NMAX * 3; i++) {
                for (int j = 0; j < 8; j++) {
                    send_bit2(bit_read(stripe1_array[i],(7-j)));
                }
            }
            break;
        };
        case 3: {
            for (int i = 0; i < NMAX * 3; i++) {
                for (int j = 0; j < 8; j++) {
                    send_bit3(bit_read(stripe2_array[i],(7-j)));
                }
            }
            break;
        };
    }
}

// initialize LED
void stripe_init() {
    set_brightness(0.5);
    DDRB |= (1 << LED0_PIN);
    DDRB |= (1 << LED1_PIN);
    DDRB |= (1 << LED2_PIN);
    reset_stripe_array();
    send_stripe_array(LED0_PIN);
    send_stripe_array(LED1_PIN);
    send_stripe_array(LED2_PIN);
}

// sets brightness
void set_brightness(float _value){
    brightness = _value;
}


struct RGB set_ball_color(int _speed){
	if (_speed <= 80) {
		ball_color = red;
	} else if (_speed <= 85) {
		ball_color = orange;
	} else if (_speed <= 90) {
		ball_color = cyan;
	} else if (_speed <= 95) {
		ball_color = nice_green;
	} else if (_speed > 95) {
		ball_color = lilac;
	}
	return ball_color;
}
