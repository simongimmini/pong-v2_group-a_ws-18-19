#ifndef LEDCOMMUNICATION_H
#define LEDCOMMUNICATION_H

#include <avr/io.h>
#include <util/delay.h>

// define register, port and pins of LED-Stripes
#define LED_DDR DDRB
#define LED_PORT PORTB

#define LED0_PIN 1
#define LED1_PIN 2
#define LED2_PIN 3

// maximum of LEDs per stripe
#define NMAX 32

/* defines the bit functions and the singal lengths (times in ns) */
#define T0H 200
#define T0L 0
#define T1H 500
#define T1L 0
#define delay_ns(ns) (__builtin_avr_delay_cycles((double)F_CPU/1000000000*ns))

#define bit_read(x,n) ((x) >> (n)) &1
//#define bit_clear(x,n) ((x) &= (~(1) << (n)))
#define bit_clear(x,n) ((x) &= ~(1<<n))
#define bit_set(x,n) ((x) |= (1<<(n)))

struct RGB {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

// some colors predefined for better reading in code
static struct RGB blank = {0,0,0};
static struct RGB white = {255,255,255};
static struct RGB red = {255,0,0};
static struct RGB orange = {253, 106, 2};
static struct RGB pink = {255,0,255};
static struct RGB green = {0,255,0};
static struct RGB blue = {0,0,255};
static struct RGB cyan = {0,255,255};
static struct RGB lilac = {104,34,139};
static struct RGB nice_green = {40,215,40};
static struct RGB ball_color = {0, 0, 0};

// array containing LED information (3 values for each LED)
unsigned int stripe0_array[(NMAX)*3+1];
unsigned int stripe1_array[(NMAX)*3+1];
unsigned int stripe2_array[(NMAX)*3+1];

// float that sets the brightness (1 is maximum)
float brightness;
float standard_brightness;

// sends one bit through the chosen pin
void send_bit1(int _delay);
void send_bit2(int _delay);
void send_bit3(int _delay);

// sets the 3 values of the N-th pixel in the Array
void set_pixel(unsigned char _R, unsigned char _G, unsigned char _B,
               unsigned char _N, int _stripe_pin);

// gets rgb color to set at N-th pixel
void set_rgb_pixel(struct RGB _rgb, unsigned char _N, int _stripe_pin);

void reset_stripe_array();
int number_of_pixels();
void send_stripe_array(int _pin);
void stripe_init();
void set_brightness(float _value);
// function to set the ball color, gets speed as parameter to determine color by speed
struct RGB set_ball_color(int _speed);

#endif // LEDCOMMUNICATION_H
