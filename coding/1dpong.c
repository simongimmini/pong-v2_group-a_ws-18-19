#define NMAX 32
#define X_CORDMAX 31		// please use a multiple of 2
#define F_CPU 16000000		// only needed if stripeinit and buttoninit don't work

#include <avr/io.h>
#include <util/delay.h>
#include "buttoncontrol.h"
#include "ledcommunication.h"
#include "gamecontrol.h"
#include "oneplayer.h"
#include "twoplayer.h"
#include "threeplayer.h"
#include "menu.h"

void run_menu();

void run_game_1player() {
		speed = 75;
		coord.led = 29;
		coord.stripe = LED0_PIN;
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		direction = -1;
		_delay_ms(200);
		while(button0_state == 0) {
			button_check(b00, &button0_state);
			button_check(b01, &button1_state);
			if(button1_state == 1) {
				_delay_ms(300);
				run_menu();
			}
		}
		while(button1_state == 0) {
			reset_stripe_array();
			move_ball_1player();
			set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
			send_stripe_array(LED0_PIN);
			button_check(b01, &button1_state);
		}
		_delay_ms(300);
		run_menu();
}

void run_game_2player_1(){
	determine_start_player(2);
	game_start(2);
	while(1) {
		reset_stripe_array();
		move_ball_2player(game_choice);
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		update_score_2player();
		if(current_2player_score == 0) {
			show_score(2);
			_delay_ms(1000);
			win_animation(LED0_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		}
		if(current_2player_score == 32) {
			show_score(2);
			_delay_ms(1000);
			win_animation(LED1_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		}
	}
}

void run_game_2player_2(){
	determine_start_player(2);
	game_start(2);
	while(1) {
		reset_stripe_array();
		move_ball2_2player();
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		check_powerup(2);
		if(current_2player_score == 0) {
			show_score(2);
			_delay_ms(1000);
			win_animation(LED0_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		}
		if(current_2player_score == 32) {
			show_score(2);
			_delay_ms(1000);
			win_animation(LED1_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		}

	}
}

void run_game_3player_1(){
	determine_start_player(3);
	game_start(3);
	while(1) {
		reset_stripe_array();
		move_ball_3player(game_choice);
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		update_score_3player();
		if (player2_points == 0 && player3_points == 0) {
			show_score(3);
			_delay_ms(1000);
			win_animation(LED0_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		} else if (player1_points == 0 && player3_points == 0) {
			show_score(3);
			_delay_ms(1000);
			win_animation(LED1_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		} else if (player1_points == 0 && player2_points == 0) {
			show_score(3);
			_delay_ms(1000);
			win_animation(LED2_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		}
	}
}

void run_game_3player_2(){
	determine_start_player(3);
	game_start(3);
	while(1) {
		reset_stripe_array();
		move_ball2_3player(game_choice);
		set_rgb_pixel(set_ball_color(speed), coord.led, coord.stripe);
		send_stripe_array(LED0_PIN);
		send_stripe_array(LED1_PIN);
		send_stripe_array(LED2_PIN);
		if (player2_points == 0 && player3_points == 0) {
			show_score(3);
			_delay_ms(1000);
			win_animation(LED0_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		} else if (player1_points == 0 && player3_points == 0) {
			show_score(3);
			_delay_ms(1000);
			win_animation(LED1_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		} else if (player1_points == 0 && player2_points == 0) {
			show_score(3);
			_delay_ms(1000);
			win_animation(LED2_PIN);
			_delay_ms(1000);
			run_menu();
			break;
		}
	}
}

void run_menu() {
	reset_stripe_array();
	send_stripe_array(LED0_PIN);
	send_stripe_array(LED1_PIN);
	send_stripe_array(LED2_PIN);
	turn_off_light_powerup(0);
	turn_off_light_powerup(1);
	turn_off_light_powerup(2);
	player_menu();
	switch (player_number) {
    	case 1:	run_game_1player();
        		break;
        case 2: switch (game_choice) {
        			case 1: run_game_2player_1();
        					break;
        			case 2: run_game_2player_1();
        					break;
        			case 3: run_game_2player_2();
        					break;
    			} break;
        case 3: switch (game_choice) {
        			case 1: run_game_3player_1();
        					break;
        			case 2: run_game_3player_1();
        					break;
        			case 3: run_game_3player_2();
        					break;
    			} break;
    }
}

int main(void) {
	// Lights up every stripe
	button_init();
	stripe_init();
	led_button_init();
	reset_stripe_array();
	//start_animation();
	demo_animation(5);
	run_menu();
}
