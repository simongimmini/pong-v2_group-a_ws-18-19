# IAP WS18-19
##### Project to maintain code of the internship and show development progress. #####

# **gitlab commands you should know:**
### check available local branches
`git branch`
### change current working branch to another one
`git checkout <your_branch_name>`
### current branches status compared to remote branch
`git status`
### add changed files to "commit-history"
`git add <your_file_name>`
### discard changes in file
`git checkout <your_file_name>`
### commit changes to branch
`git commit -m "<your_super_important_message>"`
### push commits to remote branch
`git push`
### pull the latest code from remote branch
`git pull`
### create and change to new local branch
`git checkout -B <your_new_branch>`
### push local branch and link to remote one
`git push --set-upstream origin <your_branch>`
### delete local branch
`git branch -D <your_branch>`
### reset local branch to last pull - just in case!
`git reset --hard origin/<your_branch>`

# **Commands to establish connection to hardware in the cellar of Mathematikon:**
##### connect via VPN to the University Network and use ssh to gain access to one of the 4 PC
### Connect to preston:
`ssh 2018ws_pong_a@129.206.108.117`
### Connect to rodney:
`ssh 2018ws_pong_a@129.206.108.114`
### Connect to bishop:
`ssh 2018ws_pong_a@129.206.108.127`
### Connect to daryl:
`ssh 2018ws_pong_a@129.206.108.128`
##### there will be a prompt to type in the given password. After doing so you're connected to one of these PC, nice :)
